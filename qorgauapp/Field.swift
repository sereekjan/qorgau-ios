//
//  Field.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/22/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

struct Field : Decodable {
    var raw: String?
    var rendered: String
}
