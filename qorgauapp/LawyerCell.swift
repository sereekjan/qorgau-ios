//
//  LawyerCell.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/18/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class LawyerCell : UITableViewCell {
    var lawyer: Lawyer?
    var indexOfRow: Int?
    
    let underlinedText: [NSAttributedString.Key: Any] = [
        .font: UIFont.systemFont(ofSize: 14),
        .foregroundColor: UIColor.black,
        .underlineStyle: NSUnderlineStyle.single.rawValue
    ]
    
    var regionIdLabel: UILabel = {
        var view = UILabel()
        view.backgroundColor = UIColor.init(white: 1.0, alpha: 0.0)
        view.font = UIFont.systemFont(ofSize: 14.0)
        view.textColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var regionNameLabel: UILabel = {
        var view = UILabel()
        view.backgroundColor = UIColor.init(white: 1.0, alpha: 0.0)
        view.font = UIFont.systemFont(ofSize: 14.0)
        view.textColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var regionView: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.init(red: 165.0 / 255.0, green: 82.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var addressView: UILabel = {
        var view = UILabel()
        view.font = .systemFont(ofSize: 12.0)
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var emailButton: UIButton = {
        var view = UIButton()
        view.titleLabel?.font = .systemFont(ofSize: 14.0)
        view.setTitleColor(.black, for: .normal)
        view.contentHorizontalAlignment = .left
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var phoneButton: UIButton = {
        var view = UIButton()
        view.titleLabel?.font = .systemFont(ofSize: 16.0)
        view.setTitleColor(.black, for: .normal)
        view.contentHorizontalAlignment = .left
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var contactView: UIView = {
        var view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var clockImageView: UIImageView = {
        var view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "ic_watch_later_24px")
        return view
    }()
    
    var worktimeView: UILabel = {
        var view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 12.0)
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.textColor = .darkGray
        return view
    }()
    
    var lunchtimeView: UILabel = {
        var view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 12.0)
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.textColor = .gray
        return view
    }()
    
    var scheduleView: UIView = {
        var view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var divider: UIView = {
        var view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 100, height: 2)
        view.backgroundColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var nameView: UILabel = {
        var view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = UIColor.init(red: 165.0 / 255.0, green: 82.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
        view.font = .systemFont(ofSize: 16.0)
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.regionView.addSubview(self.regionIdLabel)
        self.regionView.addSubview(self.regionNameLabel)

        self.addSubview(self.regionView)
        self.addSubview(self.contactView)
        self.addSubview(self.scheduleView)
        self.addSubview(self.divider)
        self.addSubview(self.nameView)
        
        self.contactView.addSubview(self.addressView)
        self.contactView.addSubview(self.emailButton)
        self.contactView.addSubview(self.phoneButton)
        
        self.scheduleView.addSubview(self.clockImageView)
        self.scheduleView.addSubview(self.worktimeView)
        self.scheduleView.addSubview(self.lunchtimeView)

        self.regionIdLabel.leftAnchor.constraint(equalTo: self.regionView.leftAnchor, constant: 16.0).isActive = true
        self.regionIdLabel.topAnchor.constraint(equalTo: self.regionView.topAnchor).isActive = true
        self.regionIdLabel.bottomAnchor.constraint(equalTo: self.regionView.bottomAnchor).isActive = true
        self.regionIdLabel.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        
        self.regionNameLabel.leftAnchor.constraint(equalTo: regionIdLabel.rightAnchor).isActive = true
        self.regionNameLabel.topAnchor.constraint(equalTo: self.regionView.topAnchor).isActive = true
        self.regionNameLabel.rightAnchor.constraint(equalTo: self.regionView.rightAnchor).isActive = true
        self.regionNameLabel.bottomAnchor.constraint(equalTo: self.regionView.bottomAnchor).isActive = true
        
        self.regionView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        self.regionView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.regionView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.regionView.heightAnchor.constraint(equalToConstant: 36.0).isActive = true
        
        self.addressView.leftAnchor.constraint(equalTo: self.contactView.leftAnchor).isActive = true
        self.addressView.topAnchor.constraint(equalTo: self.contactView.topAnchor).isActive = true
        self.addressView.rightAnchor.constraint(equalTo: self.contactView.rightAnchor).isActive = true
        self.addressView.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        
        self.emailButton.leftAnchor.constraint(equalTo: self.contactView.leftAnchor).isActive = true
        self.emailButton.topAnchor.constraint(equalTo: self.addressView.bottomAnchor).isActive = true
        self.emailButton.rightAnchor.constraint(equalTo: self.contactView.rightAnchor).isActive = true
        self.emailButton.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        
        self.phoneButton.leftAnchor.constraint(equalTo: self.contactView.leftAnchor).isActive = true
        self.phoneButton.topAnchor.constraint(equalTo: self.emailButton.bottomAnchor, constant: 8.0).isActive = true
        self.phoneButton.rightAnchor.constraint(equalTo: self.contactView.rightAnchor).isActive = true
        self.phoneButton.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        
        self.contactView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 48.0).isActive = true
        self.contactView.topAnchor.constraint(equalTo: self.regionView.bottomAnchor, constant: 8.0).isActive = true
        self.contactView.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: 16.0).isActive = true
        self.contactView.heightAnchor.constraint(equalToConstant: 72.0).isActive = true
        
        self.clockImageView.leftAnchor.constraint(equalTo: self.scheduleView.leftAnchor).isActive = true
        self.clockImageView.topAnchor.constraint(equalTo: self.scheduleView.topAnchor, constant: 8.0).isActive = true
        self.clockImageView.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
        self.clockImageView.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
        
        self.worktimeView.leftAnchor.constraint(equalTo: self.clockImageView.rightAnchor, constant: 12.0).isActive = true
        self.worktimeView.topAnchor.constraint(equalTo: self.scheduleView.topAnchor, constant: 8.0).isActive = true
        self.worktimeView.rightAnchor.constraint(equalTo: self.scheduleView.rightAnchor).isActive = true
        self.worktimeView.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        
        self.lunchtimeView.leftAnchor.constraint(equalTo: self.worktimeView.leftAnchor).isActive = true
        self.lunchtimeView.topAnchor.constraint(equalTo: self.worktimeView.bottomAnchor).isActive = true
        self.lunchtimeView.rightAnchor.constraint(equalTo: self.scheduleView.rightAnchor).isActive = true
        self.lunchtimeView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        
        self.scheduleView.leftAnchor.constraint(equalTo: self.contactView.rightAnchor).isActive = true
        self.scheduleView.topAnchor.constraint(equalTo: self.regionView.bottomAnchor, constant: 8.0).isActive = true
        self.scheduleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16.0).isActive = true
        self.scheduleView.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        
        self.divider.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 48.0).isActive = true
        self.divider.topAnchor.constraint(equalTo: self.contactView.bottomAnchor, constant: 8.0).isActive = true
        self.divider.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16.0).isActive = true
        self.divider.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        
        self.nameView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 48.0).isActive = true
        self.nameView.topAnchor.constraint(equalTo: self.divider.bottomAnchor, constant: 12.0).isActive = true
        self.nameView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16.0).isActive = true
        self.nameView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        self.nameView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16.0).isActive = true
        
        self.emailButton.addTarget(self, action: #selector(onEmailButtonClicked(sender:)), for: .touchUpInside)
        self.phoneButton.addTarget(self, action: #selector(onPhoneButtonClicked(sender:)), for: .touchUpInside)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let lawyer = lawyer {
            self.regionNameLabel.text = lawyer.region.isEmpty ? "Нет адреса".localiz() : lawyer.region
            self.nameView.text = lawyer.fio.isEmpty ? "Нет данных" : lawyer.fio
            self.addressView.text = lawyer.address.isEmpty ? "Нет адреса".localiz() : lawyer.address
            if lawyer.email.isEmpty {
                self.emailButton.setTitle("Нет почты".localiz(), for: .normal)
            } else {
                self.emailButton.setAttributedTitle(NSMutableAttributedString(string: lawyer.email, attributes: underlinedText), for: .normal)
            }
            if lawyer.phone.isEmpty {
                self.phoneButton.setTitle("Нет телефона".localiz(), for: .normal)
            } else {
                self.phoneButton.setTitle(lawyer.phone, for: .normal)
            }
            self.worktimeView.text = lawyer.schedule.isEmpty ? "Нет расписания".localiz() : lawyer.schedule
            self.lunchtimeView.text = lawyer.lunch
        }
        
        if let indexOfRow = indexOfRow {
            self.regionIdLabel.text = "\(indexOfRow + 1)"
        }
    }
    
    @objc func onEmailButtonClicked(sender: UIButton!) {
        if let email = lawyer?.email, !email.isEmpty {
            startApp(URL(string: "sendto://\(email)")!)
        }
    }
    
    @objc func onPhoneButtonClicked(sender: UIButton!) {
        if let phone = lawyer?.phone, !phone.isEmpty {
            startApp(URL(string: "tel://\(phone)")!)
        }
    }
    
    private func startApp(_ url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
