//
//  MenuAboutViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/28/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class MenuAboutViewController : UIViewController {
    
    var viewTitle: String?
    var filename: String?
    
    var menuButton: UIButton = {
        var view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "ic_arrow_back"), for: .normal)
        return view
    }()
    
    var headerView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        return view
    }()
    
    var titleView: UILabel = {
        var view = UILabel()
        view.font = .systemFont(ofSize: 24.0, weight: .bold)
        view.textColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var contentView: WKWebView = {
        var view = WKWebView()
        view.scalesLargeContentImage = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        self.view.addSubview(self.headerView)
        self.headerView.addSubview(self.menuButton)
        self.headerView.addSubview(self.titleView)
        self.view.addSubview(self.contentView)
        
        let safeGuide = self.view.safeAreaLayoutGuide
        
        self.headerView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.headerView.topAnchor.constraint(equalTo: safeGuide.topAnchor).isActive = true
        self.headerView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.headerView.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        
        self.menuButton.leftAnchor.constraint(equalTo: self.headerView.leftAnchor, constant: 16.0).isActive = true
        self.menuButton.bottomAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: -16.0).isActive = true
        self.menuButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.menuButton.addTarget(self, action: #selector(onBackPressed(sender:)), for: .touchUpInside)
        
        self.titleView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.titleView.centerYAnchor.constraint(equalTo: self.menuButton.centerYAnchor).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 26.0).isActive = true
        
        self.contentView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor).isActive = true
        self.contentView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor).isActive = true
        
        self.titleView.text = self.viewTitle
        
        if let url = Bundle.main.url(forResource: self.filename, withExtension: "html") {
            self.contentView.loadFileURL(url, allowingReadAccessTo: url)
//            self.contentView.load(URLRequest(url: url))
        }
    }
    
    @objc func onBackPressed(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
