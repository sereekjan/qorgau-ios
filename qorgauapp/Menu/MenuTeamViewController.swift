//
//  MenuTeamViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/28/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class MenuTeamViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cellIdentifier = "TeamCell"
        
    var menuButton: UIButton = {
        var view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "ic_arrow_back"), for: .normal)
        return view
    }()
    
    var headerView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var titleView: UILabel = {
        var view = UILabel()
        view.font = .systemFont(ofSize: 24.0, weight: .bold)
        view.textColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var team: [TeamMember] = [
        TeamMember(image: "man1", name: "Лепсибаев Чингиз Коктемсерикович", position: "Проект-менеджер"),
        TeamMember(image: "man2", name: "Сагынбай Баглан Есентайулы", position: "Старший менеджер"),
        TeamMember(image: "man3", name: "Усембаева Айгерим", position: "Менеджер мероприятий"),
        TeamMember(image: "man4", name: "Тузельбаев Гамал Бекболатович", position: "Менеджер по работе с МРЦ"),
        TeamMember(image: "man5", name: "Досова Гульнар Женисбековна", position: "Координатор-делопроизводитель"),
        TeamMember(image: "man6", name: "Пулатов Шерзод Аббозович", position: "Консультант в сфере защиты прав человека"),
        TeamMember(image: "man7", name: "Кусаинов Жомарт Алпысбаевич", position: "Консультант в сфере защиты прав человека"),
        TeamMember(image: "man8", name: "Байболатова Галия Кизатовна", position: "Менеджер проекта по регионам"),
        TeamMember(image: "man9", name: "Ленчук Елена", position: "Помощник бухгалтера"),
        TeamMember(image: "man10", name: "Бежибаев Азамат Ануарулы", position: "Менеджер по работе с НПО")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        self.view.addSubview(self.headerView)
        self.headerView.addSubview(self.menuButton)
        self.headerView.addSubview(self.titleView)
        self.view.addSubview(self.tableView)
        
        let safeArea = self.view.safeAreaLayoutGuide
        
        self.headerView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.headerView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        self.headerView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.headerView.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        
        self.menuButton.leftAnchor.constraint(equalTo: self.headerView.leftAnchor, constant: 16.0).isActive = true
        self.menuButton.bottomAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: -16.0).isActive = true
        self.menuButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.menuButton.addTarget(self, action: #selector(onBackPressed(sender:)), for: .touchUpInside)
        
        self.titleView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.titleView.centerYAnchor.constraint(equalTo: self.menuButton.centerYAnchor).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 26.0).isActive = true
        self.titleView.text = "Наша команда".localiz()
        
        self.tableView.register(TeamCell.self, forCellReuseIdentifier: cellIdentifier)
        self.tableView.separatorColor = .white
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
    }
    
    @objc func onBackPressed(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return team.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! TeamCell
        
        cell.teamMember = team[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96.0
    }
}
