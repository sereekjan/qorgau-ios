//
//  TeamMember.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/28/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

struct TeamMember {
    var image: String
    var name: String
    var position: String
}
