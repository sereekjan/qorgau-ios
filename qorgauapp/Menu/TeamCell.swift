//
//  TeamCell.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/28/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class TeamCell : UITableViewCell {
    var teamMember: TeamMember?
    
    var avatarView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var nameView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.textColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.lineBreakMode = .byWordWrapping
        view.font = .systemFont(ofSize: 16.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var positionView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.font = .systemFont(ofSize: 13.0, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(self.avatarView)
        self.addSubview(self.nameView)
        self.addSubview(self.positionView)
        
        self.avatarView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16.0).isActive = true
        self.avatarView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
        self.avatarView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0).isActive = true
        self.avatarView.heightAnchor.constraint(equalToConstant: 96.0).isActive = true
        self.avatarView.widthAnchor.constraint(equalToConstant: 96.0).isActive = true
        
        self.nameView.leftAnchor.constraint(equalTo: self.avatarView.rightAnchor, constant: 16.0).isActive = true
        self.nameView.topAnchor.constraint(equalTo: self.avatarView.topAnchor, constant: 8.0).isActive = true
        self.nameView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16.0).isActive = true
        self.nameView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        self.positionView.leftAnchor.constraint(equalTo: self.avatarView.rightAnchor, constant: 16.0).isActive = true
        self.positionView.topAnchor.constraint(equalTo: self.nameView.bottomAnchor).isActive = true
        self.positionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16.0).isActive = true
        self.positionView.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
    }
    
    override func layoutSubviews() {
        if let teamMember = self.teamMember {
            self.avatarView.image = UIImage(named: teamMember.image)
            self.nameView.text = teamMember.name
            self.positionView.text = teamMember.position
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
