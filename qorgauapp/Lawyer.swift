//
//  Lawyer.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/18/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

struct Lawyer : Decodable {
    var id: Int
    var group: Int
    var region: String
    var fio: String
    var address: String
    var email: String
    var phone: String
    var schedule: String
    var lunch: String
}
