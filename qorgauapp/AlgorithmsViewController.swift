//
//  AlgorithmsViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/29/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class AlgorithmsViewController : UIViewController {
        
    var menuButton: UIButton = {
        var view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "ic_sort_black_24px"), for: .normal)
        return view
    }()
    
    var headerView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var titleView: UILabel = {
        var view = UILabel()
        view.text = "Qorgau"
        view.font = .systemFont(ofSize: 24.0, weight: .bold)
        view.textColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var firstImageView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "algorithm_bg")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var firstIconView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "ic_grandparents")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var firstTitleView: UILabel = {
        var view = UILabel()
        view.textColor = .white
        view.numberOfLines = 2
        view.contentMode = .bottom
        view.textAlignment = .right
        view.font = .systemFont(ofSize: 12.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var secondImageView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "algorithm_bg")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var secondIconView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "ic_girl")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var secondTitleView: UILabel = {
        var view = UILabel()
        view.textColor = .white
        view.numberOfLines = 2
        view.contentMode = .bottom
        view.textAlignment = .right
        view.font = .systemFont(ofSize: 12.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var thirdImageView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "algorithm_bg")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var thirdIconView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "ic_radiation")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var thirdTitleView: UILabel = {
        var view = UILabel()
        view.textColor = .white
        view.numberOfLines = 3
        view.contentMode = .bottom
        view.textAlignment = .right
        view.font = .systemFont(ofSize: 12.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var forthImageView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "algorithm_bg")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var forthIconView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "ic_man")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var forthTitleView: UILabel = {
        var view = UILabel()
        view.textColor = .white
        view.numberOfLines = 2
        view.contentMode = .bottom
        view.textAlignment = .right
        view.font = .systemFont(ofSize: 12.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var fifthImageView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "algorithm_bg")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var fifthIconView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "ic_laborers")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var fifthTitleView: UILabel = {
        var view = UILabel()
        view.textColor = .white
        view.numberOfLines = 2
        view.contentMode = .bottom
        view.textAlignment = .right
        view.font = .systemFont(ofSize: 12.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var sixthImageView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "algorithm_bg")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var sixthIconView: UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "ic_stop_violence")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var sixthTitleView: UILabel = {
        var view = UILabel()
        view.textColor = .white
        view.numberOfLines = 3
        view.contentMode = .bottom
        view.textAlignment = .right
        view.font = .systemFont(ofSize: 12.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        let safeGuide = self.view.safeAreaLayoutGuide
        
        self.view.addSubview(self.headerView)
        self.headerView.addSubview(self.menuButton)
        self.headerView.addSubview(self.titleView)
        
        self.view.addSubview(self.firstImageView)
        self.view.addSubview(self.firstIconView)
        self.view.addSubview(self.firstTitleView)
        
        self.view.addSubview(self.secondImageView)
        self.view.addSubview(self.secondIconView)
        self.view.addSubview(self.secondTitleView)
        
        self.view.addSubview(self.thirdImageView)
        self.view.addSubview(self.thirdIconView)
        self.view.addSubview(self.thirdTitleView)
        
        self.view.addSubview(self.forthImageView)
        self.view.addSubview(self.forthIconView)
        self.view.addSubview(self.forthTitleView)
        
        self.view.addSubview(self.fifthImageView)
        self.view.addSubview(self.fifthIconView)
        self.view.addSubview(self.fifthTitleView)
        
        self.view.addSubview(self.sixthImageView)
        self.view.addSubview(self.sixthIconView)
        self.view.addSubview(self.sixthTitleView)
        
        self.headerView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.headerView.topAnchor.constraint(equalTo: safeGuide.topAnchor).isActive = true
        self.headerView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.headerView.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        
        self.menuButton.leftAnchor.constraint(equalTo: self.headerView.leftAnchor, constant: 16.0).isActive = true
        self.menuButton.bottomAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: -16.0).isActive = true
        self.menuButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.menuButton.addTarget(self, action: #selector(onMenuClicked(sender:)), for: .touchUpInside)
        
        self.titleView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.titleView.centerYAnchor.constraint(equalTo: self.menuButton.centerYAnchor).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 26.0).isActive = true
        
        self.firstImageView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.firstImageView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: 24.0).isActive = true
        self.firstImageView.rightAnchor.constraint(equalTo: self.view.centerXAnchor, constant: -6.0).isActive = true
        self.firstImageView.heightAnchor.constraint(equalTo: self.firstImageView.widthAnchor, multiplier: 1.0 / 1.0).isActive = true
        
        self.firstIconView.centerXAnchor.constraint(equalTo: self.firstImageView.centerXAnchor).isActive = true
        self.firstIconView.centerYAnchor.constraint(equalTo: self.firstImageView.centerYAnchor, constant: -16.0).isActive = true
        self.firstIconView.widthAnchor.constraint(equalToConstant: 52.0).isActive = true
        self.firstIconView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.firstTitleView.leftAnchor.constraint(equalTo: self.firstImageView.leftAnchor, constant: 16.0).isActive = true
        self.firstTitleView.rightAnchor.constraint(equalTo: self.firstImageView.rightAnchor, constant: -16.0).isActive = true
        self.firstTitleView.bottomAnchor.constraint(equalTo: self.firstImageView.bottomAnchor, constant: -16.0).isActive = true
        self.firstTitleView.text = "Защита прав\nинвалидов".localiz()
        
        self.secondImageView.leftAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 6.0).isActive = true
        self.secondImageView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: 24.0).isActive = true
        self.secondImageView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.secondImageView.heightAnchor.constraint(equalTo: self.firstImageView.widthAnchor, multiplier: 1.0 / 1.0).isActive = true
        
        self.secondIconView.centerXAnchor.constraint(equalTo: self.secondImageView.centerXAnchor).isActive = true
        self.secondIconView.centerYAnchor.constraint(equalTo: self.secondImageView.centerYAnchor, constant: -16.0).isActive = true
        self.secondIconView.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.secondIconView.heightAnchor.constraint(equalToConstant: 49.5).isActive = true
        
        self.secondTitleView.leftAnchor.constraint(equalTo: self.secondImageView.leftAnchor, constant: 16.0).isActive = true
        self.secondTitleView.rightAnchor.constraint(equalTo: self.secondImageView.rightAnchor, constant: -16.0).isActive = true
        self.secondTitleView.bottomAnchor.constraint(equalTo: self.secondImageView.bottomAnchor, constant: -16.0).isActive = true
        self.secondTitleView.text = "Защита прав\nдетей".localiz()
        
        self.thirdImageView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.thirdImageView.topAnchor.constraint(equalTo: self.secondTitleView.bottomAnchor, constant: 24.0).isActive = true
        self.thirdImageView.rightAnchor.constraint(equalTo: self.view.centerXAnchor, constant: -6.0).isActive = true
        self.thirdImageView.heightAnchor.constraint(equalTo: self.thirdImageView.widthAnchor, multiplier: 1.0 / 1.0).isActive = true
        
        self.thirdIconView.centerXAnchor.constraint(equalTo: self.thirdImageView.centerXAnchor).isActive = true
        self.thirdIconView.centerYAnchor.constraint(equalTo: self.thirdImageView.centerYAnchor, constant: -16.0).isActive = true
        self.thirdIconView.widthAnchor.constraint(equalToConstant: 52.0).isActive = true
        self.thirdIconView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.thirdTitleView.leftAnchor.constraint(equalTo: self.thirdImageView.leftAnchor, constant: 16.0).isActive = true
        self.thirdTitleView.rightAnchor.constraint(equalTo: self.thirdImageView.rightAnchor, constant: -16.0).isActive = true
        self.thirdTitleView.bottomAnchor.constraint(equalTo: self.thirdImageView.bottomAnchor, constant: -16.0).isActive = true
        self.thirdTitleView.text = "Защита прав\nпострадавших от\nядерных испытаний".localiz()
        
        self.forthImageView.leftAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 6.0).isActive = true
        self.forthImageView.topAnchor.constraint(equalTo: self.secondTitleView.bottomAnchor, constant: 24.0).isActive = true
        self.forthImageView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.forthImageView.heightAnchor.constraint(equalTo: self.forthImageView.widthAnchor, multiplier: 1.0 / 1.0).isActive = true
        
        self.forthIconView.centerXAnchor.constraint(equalTo: self.forthImageView.centerXAnchor).isActive = true
        self.forthIconView.centerYAnchor.constraint(equalTo: self.forthImageView.centerYAnchor, constant: -16.0).isActive = true
        self.forthIconView.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.forthIconView.heightAnchor.constraint(equalToConstant: 49.5).isActive = true
        
        self.forthTitleView.leftAnchor.constraint(equalTo: self.forthImageView.leftAnchor, constant: 16.0).isActive = true
        self.forthTitleView.rightAnchor.constraint(equalTo: self.forthImageView.rightAnchor, constant: -16.0).isActive = true
        self.forthTitleView.bottomAnchor.constraint(equalTo: self.forthImageView.bottomAnchor, constant: -16.0).isActive = true
        self.forthTitleView.text = "Защита прав\nпенсионеров".localiz()
        
        self.fifthImageView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.fifthImageView.topAnchor.constraint(equalTo: self.forthTitleView.bottomAnchor, constant: 24.0).isActive = true
        self.fifthImageView.rightAnchor.constraint(equalTo: self.view.centerXAnchor, constant: -6.0).isActive = true
        self.fifthImageView.heightAnchor.constraint(equalTo: self.fifthImageView.widthAnchor, multiplier: 1.0 / 1.0).isActive = true
        
        self.fifthIconView.centerXAnchor.constraint(equalTo: self.fifthImageView.centerXAnchor).isActive = true
        self.fifthIconView.centerYAnchor.constraint(equalTo: self.fifthImageView.centerYAnchor, constant: -16.0).isActive = true
        self.fifthIconView.widthAnchor.constraint(equalToConstant: 52.0).isActive = true
        self.fifthIconView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.fifthTitleView.leftAnchor.constraint(equalTo: self.fifthImageView.leftAnchor, constant: 16.0).isActive = true
        self.fifthTitleView.rightAnchor.constraint(equalTo: self.fifthImageView.rightAnchor, constant: -16.0).isActive = true
        self.fifthTitleView.bottomAnchor.constraint(equalTo: self.fifthImageView.bottomAnchor, constant: -16.0).isActive = true
        self.fifthTitleView.text = "Защита прав\nработника".localiz()
        
        self.sixthImageView.leftAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 6.0).isActive = true
        self.sixthImageView.topAnchor.constraint(equalTo: self.forthTitleView.bottomAnchor, constant: 24.0).isActive = true
        self.sixthImageView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.sixthImageView.heightAnchor.constraint(equalTo: self.sixthImageView.widthAnchor, multiplier: 1.0 / 1.0).isActive = true
        
        self.sixthIconView.centerXAnchor.constraint(equalTo: self.sixthImageView.centerXAnchor).isActive = true
        self.sixthIconView.centerYAnchor.constraint(equalTo: self.sixthImageView.centerYAnchor, constant: -16.0).isActive = true
        self.sixthIconView.widthAnchor.constraint(equalToConstant: 48.0).isActive = true
        self.sixthIconView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.sixthTitleView.leftAnchor.constraint(equalTo: self.sixthImageView.leftAnchor, constant: 16.0).isActive = true
        self.sixthTitleView.rightAnchor.constraint(equalTo: self.sixthImageView.rightAnchor, constant: -16.0).isActive = true
        self.sixthTitleView.bottomAnchor.constraint(equalTo: self.sixthImageView.bottomAnchor, constant: -16.0).isActive = true
        self.sixthTitleView.text = "Защита прав жертв\nдомашнего насилия".localiz()

        NotificationCenter.default.addObserver(self, selector: #selector(langChanged(_:)), name: Notification.Name("LangChanged"), object: nil)
    }
    
    @objc func onMenuClicked(sender: UIButton?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MenuOpen"), object: nil)
    }
    
    @objc func langChanged(_ notification: Notification) {
        self.viewDidLoad()
    }
}
