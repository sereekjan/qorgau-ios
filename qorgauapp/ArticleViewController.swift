//
//  ArticleViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 12/10/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class ArticleViewController : UIViewController {
    
    let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"

    var article: Article?
    
    var backButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "ic_arrow_back"), for: .normal)
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onBackClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    var titleView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 22, weight: .bold)
        view.textColor = .black
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var descriptionView: WKWebView = {
        let view = WKWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let safeAreaGuide = self.view.safeAreaLayoutGuide
        
        self.view.backgroundColor = .white
        
        self.view.addSubview(self.backButton)
        self.view.addSubview(self.titleView)
        self.view.addSubview(self.descriptionView)

        self.backButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.backButton.topAnchor.constraint(equalTo: safeAreaGuide.topAnchor, constant: 52.0).isActive = true
        self.backButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
        self.backButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
        
        self.titleView.leftAnchor.constraint(equalTo: backButton.rightAnchor, constant: 24.0).isActive = true
        self.titleView.topAnchor.constraint(equalTo: safeAreaGuide.topAnchor, constant: 40.0).isActive = true
        self.titleView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.descriptionView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 30.0).isActive = true
        self.descriptionView.topAnchor.constraint(equalTo: self.titleView.bottomAnchor, constant: 32.0).isActive = true
        self.descriptionView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -30.0).isActive = true
        self.descriptionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -16.0).isActive = true
        
        titleView.text = article?.title.rendered
        if let html = article?.content.rendered {
            descriptionView.loadHTMLString(headerString + html, baseURL: nil)
        }
    }
    
    @objc func onBackClicked(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

