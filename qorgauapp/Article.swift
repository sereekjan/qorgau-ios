//
//  Article.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/29/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

struct Article : Decodable {
    var id: Int
    var title: Field
    var content: Field
    var author: Int
    var date: String
}
