//
//  LoginViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/21/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class LoginViewController : UIViewController {
    
    private let AUTH_REQUEST = "https://qorgau.factum.agency/wp-json/api-bearer-auth/v1/login/"
    private let headers: HTTPHeaders = [
        "Accept": "application/json",
        "Content-Type": "application/json"
    ]
    
    var emailView: UITextField = {
        let view = UITextField()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.placeholder = "E-mail"
        view.backgroundColor = .white
        view.autocapitalizationType = .none
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var emailBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 53.0 / 255.0, green: 38.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var passwordView: UITextField = {
        let view = UITextField()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.placeholder = "Пароль".localiz()
        view.isSecureTextEntry = true
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var passwordBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 53.0 / 255.0, green: 38.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var loginButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.setTitle("Войти".localiz().uppercased(), for: .normal)
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .bold)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onAuthorizeClicked(sender:)), for: .touchUpInside)
        view.backgroundColor = UIColor.init(red: 138.0 / 255.0, green: 86.0 / 255.0, blue: 172.0 / 255.0, alpha: 1.0)
        view.layer.cornerRadius = 24.0
        return view
    }()
    
    var progressView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var errorView: UILabel = {
        let view = UILabel()
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 16)
        view.textColor = .red
        view.text = "Ошибка при попытке авторизации".localiz()
        view.font = .systemFont(ofSize: 14.0)
        view.textAlignment = .center
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var registrationButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.setTitle("Регистрация".localiz(), for: .normal)
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .bold)
        view.setTitleColor(.lightGray, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onRegistrateClicked(sender:)), for: .touchUpInside)
        view.layer.cornerRadius = 24.0
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        renderView()
    }
    
    private func renderView() {
        self.view.backgroundColor = .white
        
        self.view.addSubview(emailView)
        self.view.addSubview(emailBorder)
        self.view.addSubview(passwordView)
        self.view.addSubview(passwordBorder)
        self.view.addSubview(loginButton)
        self.view.addSubview(errorView)
        self.view.addSubview(progressView)
        self.view.addSubview(registrationButton)
        
        self.passwordView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64.0).isActive = true
        self.passwordView.topAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -32.0).isActive = true
        self.passwordView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64.0).isActive = true
        self.passwordView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        self.passwordBorder.leftAnchor.constraint(equalTo: self.passwordView.leftAnchor).isActive = true
        self.passwordBorder.topAnchor.constraint(equalTo: self.passwordView.bottomAnchor).isActive = true
        self.passwordBorder.rightAnchor.constraint(equalTo: self.passwordView.rightAnchor).isActive = true
        self.passwordBorder.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        
        self.emailView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64.0).isActive = true
        self.emailView.bottomAnchor.constraint(equalTo: self.passwordView.topAnchor, constant: -16.0).isActive = true
        self.emailView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64.0).isActive = true
        self.emailView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        self.emailBorder.leftAnchor.constraint(equalTo: self.emailView.leftAnchor).isActive = true
        self.emailBorder.topAnchor.constraint(equalTo: self.emailView.bottomAnchor).isActive = true
        self.emailBorder.rightAnchor.constraint(equalTo: self.emailView.rightAnchor).isActive = true
        self.emailBorder.heightAnchor.constraint(equalToConstant: 2.0).isActive = true

        self.loginButton.leftAnchor.constraint(equalTo: self.emailView.leftAnchor).isActive = true
        self.loginButton.topAnchor.constraint(equalTo: self.passwordView.bottomAnchor, constant: 48.0).isActive = true
        self.loginButton.rightAnchor.constraint(equalTo: self.emailView.rightAnchor).isActive = true
        self.loginButton.heightAnchor.constraint(equalToConstant: 48.0).isActive = true

        self.progressView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.progressView.topAnchor.constraint(equalTo: self.passwordView.bottomAnchor, constant: 48.0).isActive = true
        self.progressView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.progressView.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        self.progressView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        self.errorView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.errorView.topAnchor.constraint(equalTo: self.loginButton.bottomAnchor, constant: 24.0).isActive = true
        self.errorView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.errorView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        
        self.registrationButton.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.registrationButton.topAnchor.constraint(equalTo: self.errorView.bottomAnchor, constant: 16.0).isActive = true
        self.registrationButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.registrationButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
    }
    
    @objc func onAuthorizeClicked(sender: UIButton) {
//        self.loginButton.isHidden = true
        self.emailView.endEditing(true)
        self.passwordView.endEditing(true)
        self.errorView.isHidden = true
        
        sendAuthRequest(email: emailView.text ?? "", password: passwordView.text ?? "")
//        if emailView.text?.isEmpty == true {
//
//        }
    }
    
    @objc func onRegistrateClicked(sender: UIButton) {
        let registrationVC = RegistrationViewController()
        registrationVC.modalPresentationStyle = .fullScreen
        present(registrationVC, animated: false, completion: nil)
    }
    
    private func saveAccessTokenAndLeaveVC(_ authResponse: AuthResponse) {
        UserManager.shared.accessToken = authResponse.accessToken
        UserManager.shared.userId = authResponse.wpUser.id
        UserManager.shared.userLogin = authResponse.wpUser.data.login
        UserManager.shared.userEmail = authResponse.wpUser.data.email
        
        self.dismiss(animated: true, completion: nil)
    }
    
    private func sendAuthRequest(email: String, password: String) {
        self.loginButton.isHidden = true
        self.progressView.isHidden = false
        self.progressView.startAnimating()
        
        Alamofire.request(
            AUTH_REQUEST,
            method: .post,
            encoding: "{\"username\":\"\(email)\",\"password\":\"\(password)\"}",
            headers: self.headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                self.progressView.stopAnimating()
                self.loginButton.isHidden = false
                self.progressView.isHidden = true
                
                guard response.result.isSuccess else {
                    self.errorView.isHidden = false
                    return
                }
                
                do {
                    print(response.result.value)
                    let authResponse = try JSONDecoder().decode(AuthResponse.self, from: response.result.value!)
                    self.saveAccessTokenAndLeaveVC(authResponse)
                } catch { }
            }
    }
}

extension String: ParameterEncoding {

    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }

}
