//
//  ProfileViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/21/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ProfileViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let TICKETS_REQUEST = "https://qorgau.factum.agency/wp-json/wpas-api/v1/tickets"
    private var headers: HTTPHeaders {
        get {
            return ["Authorization": "Bearer \(UserManager.shared.accessToken ?? "")"]
        }
    }
    
    let identifier = "TicketCell"
    
    var tickets: [Ticket] = []
        
    var menuButton: UIButton = {
        var view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "ic_sort_black_24px"), for: .normal)
        return view
    }()
    
    var profileImageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ic_person_24px")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var profileRoundedView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.layer.cornerRadius = 64.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var profileNameView: UILabel = {
        let view = UILabel()
        view.textColor = .black
        view.font = .systemFont(ofSize: 24.0, weight: .semibold)
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var profileEmailView: UILabel = {
        let view = UILabel()
        view.textColor = .gray
        view.font = .systemFont(ofSize: 16.0, weight: .light)
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var ticketsTableView: UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorColor = .white
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var refreshControl: UIRefreshControl = {
        let view = UIRefreshControl()
        return view
    }()
    
    var progressView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var errorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var errorLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14.0)
        view.textColor = .red
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var errorButton: UIButton = {
        let view = UIButton(type: UIButton.ButtonType.system)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var logoutButton: UIButton = {
        let view = UIButton(type: UIButton.ButtonType.system)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var newTicketButton: UIButton = {
        let view = UIButton()
        view.layer.cornerRadius = 28.0
        view.setImage(UIImage(named: "ic_icon_add"), for: .normal)
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(newTicketButtonClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkIfAuthorized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(langChanged(_:)), name: Notification.Name("LangChanged"), object: nil)
    }
    
    @objc func langChanged(_ notification: Notification) {
        checkIfAuthorized()
    }
    
    func checkIfAuthorized() {
        if UserManager.shared.isAuthorized() {
            renderView()
            loadTickets()
        } else {
            openLoginVC()
        }
    }
    
    private func openLoginVC() {
        let loginVC = LoginViewController()
        loginVC.modalPresentationStyle = .fullScreen
        present(loginVC, animated: true, completion: nil)
    }
    
    private func renderView() {
        self.view.addSubview(self.profileRoundedView)
        self.profileRoundedView.addSubview(self.profileImageView)
        
        self.view.addSubview(self.profileNameView)
        self.view.addSubview(self.profileEmailView)
        self.view.addSubview(self.ticketsTableView)
        self.view.addSubview(self.progressView)
        self.view.addSubview(self.errorView)
        self.view.addSubview(self.logoutButton)
        self.view.addSubview(self.newTicketButton)
        self.view.addSubview(self.menuButton)
        
        self.errorView.addSubview(self.errorLabel)
        self.errorView.addSubview(self.errorButton)
        
        let safeAreaGuide = self.view.safeAreaLayoutGuide
        
        self.menuButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16.0).isActive = true
        self.menuButton.topAnchor.constraint(greaterThanOrEqualTo: safeAreaGuide.topAnchor, constant: 26.0).isActive = true
        self.menuButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.menuButton.addTarget(self, action: #selector(onMenuClicked(sender:)), for: .touchUpInside)
        
        self.profileRoundedView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.profileRoundedView.topAnchor.constraint(equalTo: safeAreaGuide.topAnchor, constant: 32.0).isActive = true
        self.profileRoundedView.heightAnchor.constraint(equalToConstant: 128.0).isActive = true
        self.profileRoundedView.widthAnchor.constraint(equalToConstant: 128.0).isActive = true
        
        self.profileImageView.centerXAnchor.constraint(equalTo: self.profileRoundedView.centerXAnchor).isActive = true
        self.profileImageView.centerYAnchor.constraint(equalTo: self.profileRoundedView.centerYAnchor).isActive = true
        self.profileImageView.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.profileImageView.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        
        self.profileNameView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 32.0).isActive = true
        self.profileNameView.topAnchor.constraint(equalTo: self.profileRoundedView.bottomAnchor, constant: 24.0).isActive = true
        self.profileNameView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -32.0).isActive = true
        self.profileNameView.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
        
        self.profileEmailView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 32.0).isActive = true
        self.profileEmailView.topAnchor.constraint(equalTo: self.profileNameView.bottomAnchor, constant: 4.0).isActive = true
        self.profileEmailView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -32.0).isActive = true
        self.profileEmailView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        
        self.refreshControl.addTarget(self, action: #selector(updateButtonClicked(sender:)), for: .valueChanged)
        
        self.ticketsTableView.refreshControl = self.refreshControl
        self.ticketsTableView.separatorColor = .white
        self.ticketsTableView.register(TicketCell.self, forCellReuseIdentifier: identifier)
        self.ticketsTableView.delegate = self
        self.ticketsTableView.dataSource = self
        
        self.ticketsTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.ticketsTableView.topAnchor.constraint(equalTo: self.profileEmailView.bottomAnchor, constant: 24.0).isActive = true
        self.ticketsTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.ticketsTableView.bottomAnchor.constraint(equalTo: safeAreaGuide.bottomAnchor).isActive = true
        
        self.progressView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.progressView.centerYAnchor.constraint(equalTo: self.ticketsTableView.centerYAnchor).isActive = true
        self.progressView.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        self.progressView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        self.errorView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.errorView.topAnchor.constraint(equalTo: self.profileEmailView.bottomAnchor, constant: 24.0).isActive = true
        self.errorView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.errorView.bottomAnchor.constraint(equalTo: safeAreaGuide.bottomAnchor).isActive = true
        
        self.errorLabel.leftAnchor.constraint(equalTo: self.errorView.leftAnchor, constant: 24.0).isActive = true
        self.errorLabel.centerYAnchor.constraint(equalTo: self.errorView.centerYAnchor).isActive = true
        self.errorLabel.rightAnchor.constraint(equalTo: self.errorView.rightAnchor, constant: -24.0).isActive = true
        self.errorLabel.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        self.errorLabel.text = "Ошибка при загрузке тикетов".localiz()
        
        self.errorButton.centerXAnchor.constraint(equalTo: self.errorView.centerXAnchor).isActive = true
        self.errorButton.topAnchor.constraint(equalTo: self.errorLabel.bottomAnchor, constant: 16.0).isActive = true
        self.errorButton.addTarget(self, action: #selector(updateButtonClicked(sender:)), for: .touchUpInside)
        self.errorButton.setTitle("Обновить".localiz(), for: .normal)
        
        self.logoutButton.topAnchor.constraint(equalTo: safeAreaGuide.topAnchor, constant: 16.0).isActive = true
        self.logoutButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16.0).isActive = true
        self.logoutButton.addTarget(self, action: #selector(logoutButtonClicked(sender:)), for: .touchUpInside)
        self.logoutButton.setTitle("Выйти".localiz(), for: .normal)
        
        self.newTicketButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16.0).isActive = true
        self.newTicketButton.bottomAnchor.constraint(equalTo: safeAreaGuide.bottomAnchor, constant: -16.0).isActive = true
        self.newTicketButton.heightAnchor.constraint(equalToConstant: 56.0).isActive = true
        self.newTicketButton.widthAnchor.constraint(equalToConstant: 56.0).isActive = true
        
        if let username = UserManager.shared.userLogin {
            self.profileNameView.text = "@\(username)"
        } else {
            self.profileNameView.text = "Пользователь".localiz()
        }
        
        if let email = UserManager.shared.userEmail {
            self.profileEmailView.text = email
        } else {
            self.profileEmailView.text = ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tickets.isEmpty {
            tableView.setEmptyView(title: "Нет тикетов".localiz(), message: "Создайте первый тикет".localiz())
        } else {
            tableView.restore()
        }
        
        return tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! TicketCell
        
        cell.indexOfRow = indexPath.row
        cell.ticket = tickets[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ticketVC = TicketViewController()
        ticketVC.ticket = tickets[indexPath.row]
        present(ticketVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    @objc func updateButtonClicked(sender: UIButton?) {
        loadTickets()
    }
    
    @objc func logoutButtonClicked(sender: UIButton?) {
        let alertController = UIAlertController(title: "Внимание".localiz(), message: "Вы действительно хотите выйти?".localiz(), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Отмена".localiz(), style: .cancel))
        alertController.addAction(UIAlertAction(title: "Да".localiz(), style: .default, handler: {(alert: UIAlertAction) in
            self.logout()
        }))
        self.present(alertController, animated: true)
    }
    
    @objc func onMenuClicked(sender: UIButton?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MenuOpen"), object: nil)
    }
    
    @objc func newTicketButtonClicked(sender: UIButton?) {
        openNewTicketVC()
    }
    
    private func openNewTicketVC() {
        let newTicketVC = NewTicketViewController()
        present(newTicketVC, animated: true, completion: nil)
    }
    
    func logout() {
        UserManager.shared.clearData()
        openLoginVC()
    }
    
    func showProgress() {
        self.ticketsTableView.isHidden = true
        self.errorView.isHidden = true
        self.progressView.isHidden = false
        self.progressView.startAnimating()
    }
    
    func hideProgress() {
        self.refreshControl.endRefreshing()
        self.progressView.stopAnimating()
        self.progressView.isHidden = true
    }
    
    func showError() {
        self.errorView.isHidden = false
    }
    
    func showTickets(tickets: [Ticket]) {
        self.tickets = tickets
        self.ticketsTableView.isHidden = false
        self.ticketsTableView.reloadData()
    }
    
    private func loadTickets() {
        showProgress()
        
        Alamofire.request(TICKETS_REQUEST, method: .get, headers: self.headers)
            .validate(statusCode: 200..<300)
            .responseData { response in
                self.hideProgress()
                
                guard response.result.isSuccess else {
                    debugPrint(response.error)
                    self.showError()
                    return
                }
                
                do {
                    let tickets = try JSONDecoder().decode([Ticket].self, from: response.result.value!)
                    debugPrint(tickets)
                    self.showTickets(tickets: tickets)
                } catch { }
                
            }
    }
}
