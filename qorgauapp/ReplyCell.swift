//
//  ReplyCell.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/24/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class ReplyCell : UITableViewCell {
    
    var reply: Reply?
    var isAuthor: Bool = false
    let stringToDateFormatter = DateFormatter()
    let dateToStringFormatter = DateFormatter()
    
    var messageFrame: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 16.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var messageAvatarFrame: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 14.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var messageAvatarView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ic_person_24px")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var messageAuthorView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 16.0, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var messageTextView: UILabel = {
        let view = UILabel()
        view.textColor = .black
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var messageTimeView: UILabel = {
        let view = UILabel()
        view.textColor = .black
        view.font = .systemFont(ofSize: 10.0, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        stringToDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateToStringFormatter.dateFormat = "dd/MM/yyyy' 'HH:mm"
        
        self.addSubview(messageFrame)
        
        self.messageFrame.addSubview(self.messageAvatarFrame)
        self.messageFrame.addSubview(self.messageAuthorView)
        self.messageFrame.addSubview(self.messageTextView)
        self.messageFrame.addSubview(self.messageTimeView)
        
        self.messageAvatarFrame.addSubview(self.messageAvatarView)
        
        self.messageFrame.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0).isActive = true
        self.messageFrame.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0).isActive = true
        
        self.messageAvatarFrame.topAnchor.constraint(equalTo: self.messageFrame.topAnchor, constant: 12.0).isActive = true
        self.messageAvatarFrame.heightAnchor.constraint(equalToConstant: 28.0).isActive = true
        self.messageAvatarFrame.widthAnchor.constraint(equalToConstant: 28.0).isActive = true
        
        self.messageAvatarView.centerXAnchor.constraint(equalTo: self.messageAvatarFrame.centerXAnchor).isActive = true
        self.messageAvatarView.centerYAnchor.constraint(equalTo: self.messageAvatarFrame.centerYAnchor).isActive = true
        self.messageAvatarView.heightAnchor.constraint(equalToConstant: 12.0).isActive = true
        self.messageAvatarView.widthAnchor.constraint(equalToConstant: 12.0).isActive = true
        
        self.messageAuthorView.leftAnchor.constraint(equalTo: self.messageAvatarFrame.rightAnchor, constant: 12.0).isActive = true
        self.messageAuthorView.centerYAnchor.constraint(equalTo: self.messageAvatarFrame.centerYAnchor).isActive = true
        self.messageAuthorView.rightAnchor.constraint(equalTo: self.messageFrame.rightAnchor, constant: -16.0).isActive = true
        self.messageAuthorView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        
        self.messageTextView.leftAnchor.constraint(equalTo: self.messageFrame.leftAnchor, constant: 16.0).isActive = true
        self.messageTextView.topAnchor.constraint(equalTo: self.messageAvatarFrame.bottomAnchor, constant: 12.0).isActive = true
        self.messageTextView.rightAnchor.constraint(equalTo: self.messageFrame.rightAnchor, constant: -16.0).isActive = true
//        self.messageTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 16.0).isActive = true
        
        self.messageTimeView.leftAnchor.constraint(equalTo: self.messageFrame.leftAnchor, constant: 16.0).isActive = true
        self.messageTimeView.topAnchor.constraint(equalTo: self.messageTextView.bottomAnchor, constant: 8.0).isActive = true
        self.messageTimeView.rightAnchor.constraint(equalTo: self.messageFrame.rightAnchor, constant: -16.0).isActive = true
        self.messageTimeView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        self.messageTimeView.bottomAnchor.constraint(equalTo: self.messageFrame.bottomAnchor, constant: -12.0).isActive = true
    }
    
    func renderMessageFrame() {
        if let reply = self.reply {
            var str = reply.content
                .rendered
                .replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)

            str.removeLast()
            str.removeLast()

            self.messageTextView.text = str
            self.messageAuthorView.text = isAuthor ? "Вы".localiz() : "Собеседник".localiz()
            
            if let date = stringToDateFormatter.date(from: reply.date) {
                let convertedDate = Calendar.current.date(byAdding: .hour, value: 6, to: date)!
                self.messageTimeView.text = dateToStringFormatter.string(from: convertedDate)
            } else {
                self.messageTimeView.text = ""
            }
        }
        
        if isAuthor {
            self.messageAvatarFrame.leftAnchor.constraint(equalTo: self.messageFrame.leftAnchor, constant: 16.0).isActive = true
            self.messageAvatarFrame.backgroundColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)

            self.messageAuthorView.leftAnchor.constraint(equalTo: self.messageAvatarFrame.rightAnchor, constant: 12.0).isActive = true
            self.messageAuthorView.rightAnchor.constraint(equalTo: self.messageFrame.rightAnchor, constant: -16.0).isActive = true
            self.messageAuthorView.textColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
            self.messageAuthorView.textAlignment = .left
            
            self.messageFrame.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 64.0).isActive = true
            self.messageFrame.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16.0).isActive = true
            self.messageFrame.layer.borderColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0).cgColor
            
            self.messageTextView.textAlignment = .left
            self.messageTimeView.textAlignment = .left
        } else {
            self.messageAvatarFrame.rightAnchor.constraint(equalTo: self.messageFrame.rightAnchor, constant: -16.0).isActive = true
            self.messageAvatarFrame.backgroundColor = UIColor.init(red: 22.0 / 255.0, green: 217.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0)

            self.messageAuthorView.rightAnchor.constraint(equalTo: self.messageAvatarFrame.leftAnchor, constant: -12.0).isActive = true
            self.messageAuthorView.leftAnchor.constraint(equalTo: self.messageFrame.leftAnchor, constant: 16.0).isActive = true
            self.messageAuthorView.textColor = UIColor.init(red: 22.0 / 255.0, green: 217.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0)
            self.messageAuthorView.textAlignment = .right
            
            self.messageFrame.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16.0).isActive = true
            self.messageFrame.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -64.0).isActive = true
            self.messageFrame.layer.borderColor = UIColor.init(red: 22.0 / 255.0, green: 217.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0).cgColor
            
            self.messageTextView.textAlignment = .right
            self.messageTimeView.textAlignment = .right
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
