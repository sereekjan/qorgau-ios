//
//  LawyersViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/18/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class LawyersViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var regionsPickerView = UIPickerView()
    
    var lawyersTableView = UITableView()
    var identifier = "LawyerCell"
    
//    let regionTemplate = "Регион: "
    
    var lawyers: [Lawyer] = []
    var filteredLawyers: [Lawyer] = []
    var regions: [Region] = []
    var selectedRegion: Region?
    
    func loadLawyers() {
        do {
            if let asset = NSDataAsset(name: "lawyers_ru".localiz(), bundle: Bundle.main) as? NSDataAsset, let data = asset.data as? Data {
                let jsonResult = try JSONDecoder().decode([Lawyer].self, from: data)
                self.lawyers = jsonResult
            }
        } catch {
            print(error)
        }
    }
    
    func loadRegions() {
        do {
            if let asset = NSDataAsset(name: "regions_ru".localiz(), bundle: Bundle.main) as? NSDataAsset, let data = asset.data as? Data {
                let jsonResult = try JSONDecoder().decode([Region].self, from: data)
                self.regions = jsonResult
                    .sorted(by: {$0.order < $1.order})
                
                regionLabel.text = regions[0].name + " ▼"
                selectedRegion = regions[0]
                filterLawyers(regions[0])
                
                regionsPickerView.selectRow(0, inComponent: 0, animated: false)
            }
        } catch {
            print(error)
        }
    }
    
    var regionLabel: UITextField = {
        var view = UITextField()
        view.tintColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.textColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        view.textAlignment = NSTextAlignment.right
        return view
    }()
    
    var menuButton: UIButton = {
        var view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "ic_sort_24px"), for: .normal)
        return view
    }()
    
    var headerView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadLawyers()
        loadRegions()
        createTable()

        NotificationCenter.default.addObserver(self, selector: #selector(langChanged(_:)), name: Notification.Name("LangChanged"), object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    @objc func langChanged(_ notification: Notification) {
        self.viewDidLoad()
    }
    
    func createTable() {
        
        self.headerView.addSubview(self.menuButton)
        self.headerView.addSubview(self.regionLabel)
        self.view.addSubview(self.headerView)
        
        self.headerView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.headerView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.headerView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.headerView.heightAnchor.constraint(equalToConstant: getHeaderImageHeightForCurrentDevice()).isActive = true
        
        self.menuButton.leftAnchor.constraint(equalTo: self.headerView.leftAnchor, constant: 16.0).isActive = true
        self.menuButton.bottomAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: -16.0).isActive = true
        self.menuButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.menuButton.addTarget(self, action: #selector(onMenuClicked(sender:)), for: .touchUpInside)
        
        self.regionLabel.leftAnchor.constraint(equalTo: self.menuButton.rightAnchor).isActive = true
        self.regionLabel.rightAnchor.constraint(equalTo: self.headerView.rightAnchor, constant: -16.0).isActive = true
        self.regionLabel.bottomAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: -16.0).isActive = true
        
        self.lawyersTableView = UITableView(frame: view.bounds, style: .plain)
        self.lawyersTableView.rowHeight = UITableView.automaticDimension
        self.lawyersTableView.separatorColor = .white
        self.lawyersTableView.register(LawyerCell.self, forCellReuseIdentifier: identifier)
        self.lawyersTableView.delegate = self
        self.lawyersTableView.dataSource = self
        self.lawyersTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.lawyersTableView.translatesAutoresizingMaskIntoConstraints = false
    
        view.addSubview(self.lawyersTableView)
        
        self.lawyersTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.lawyersTableView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor).isActive = true
        self.lawyersTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.lawyersTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.regionsPickerView.dataSource = self
        self.regionsPickerView.delegate = self
        
        self.regionLabel.inputView = self.regionsPickerView
        self.regionLabel.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.largeContentTitle = "Выберите регион".localiz()
        toolBar.tintColor = UIColor.init(red: 165.0 / 255.0, green: 82.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Готово".localiz(), style: UIBarButtonItem.Style.done, target: self, action: #selector(self.onDoneClicked))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Отмена".localiz(), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.onCancelClicked))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.regionLabel.inputAccessoryView = toolBar
    }
    
    func filterLawyers(_ region: Region) {
        filteredLawyers = lawyers
            .filter { (lawyer) in lawyer.group == region.id }
    }
    
    @objc func onMenuClicked(sender: UIButton?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MenuOpen"), object: nil)
    }
    
    @objc func onDoneClicked() {
        if let selectedRegion = selectedRegion {
            filterLawyers(selectedRegion)
            regionLabel.text = selectedRegion.name + " ▼"
            self.lawyersTableView.reloadData()
        }
        
        self.regionLabel.endEditing(true)
    }
    
    @objc func onCancelClicked() {
        self.regionLabel.endEditing(true)
    }
    
    func getHeaderImageHeightForCurrentDevice() -> CGFloat {
        switch UIScreen.main.nativeBounds.height {
            case 1792, 2436, 2688: // iPhone X
                return 112.0
            default: // Every other iPhone
                return 80.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredLawyers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! LawyerCell
        
        cell.lawyer = self.filteredLawyers[indexPath.row]
        cell.indexOfRow = indexPath.row
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return regions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return regions[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRegion = regions[row]
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}
