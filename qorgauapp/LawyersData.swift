//
//  LawyersData.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/18/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

struct LawyersData : Decodable {
    var lawyers: [Lawyer]
}
