//
//  AuthResponse.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/22/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

struct WPUserData : Decodable {
//    var id: Int
    var login: String
    var email: String
    
    enum CodingKeys : String, CodingKey {
//        case id = "ID"
        case login = "user_login"
        case email = "user_email"
    }
}

struct WPUser : Decodable {
    var data: WPUserData
    var id: Int
    
    enum CodingKeys : String, CodingKey {
        case data
        case id = "ID"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.data = try! container.decode(WPUserData.self, forKey: CodingKeys.data)
        self.id = try! container.decode(Int.self, forKey: CodingKeys.id)
    }
}

struct AuthResponse : Decodable {
    var accessToken: String
    var wpUser: WPUser
    
    enum CodingKeys : String, CodingKey {
        case accessToken = "access_token"
        case wpUser = "wp_user"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.accessToken = try! container.decode(String.self, forKey: CodingKeys.accessToken)
        self.wpUser = try! container.decode(WPUser.self, forKey: CodingKeys.wpUser)
    }
}
