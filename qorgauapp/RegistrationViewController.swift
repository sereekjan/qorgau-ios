//
//  RegistrationViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/28/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class RegistrationViewController : UIViewController {
    
    private let REGISTRATION_REQUEST = "https://qorgau.factum.agency/wp-json/wp/v2/users/register"
    private var headers: HTTPHeaders {
        get {
            return [
                "Authorization": "Bearer \(UserManager.shared.accessToken ?? "")",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
        }
    }
    
    var loginView: UITextField = {
        let view = UITextField()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.placeholder = "Логин".localiz()
        view.backgroundColor = .white
        view.autocapitalizationType = .none
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var loginBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 53.0 / 255.0, green: 38.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var emailView: UITextField = {
        let view = UITextField()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.placeholder = "E-mail"
        view.backgroundColor = .white
        view.autocapitalizationType = .none
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var emailBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 53.0 / 255.0, green: 38.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var passwordView: UITextField = {
        let view = UITextField()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.placeholder = "Пароль".localiz()
        view.isSecureTextEntry = true
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var passwordBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 53.0 / 255.0, green: 38.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var confirmPasswordView: UITextField = {
        let view = UITextField()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.placeholder = "Подтвердите пароль".localiz()
        view.isSecureTextEntry = true
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var confirmPasswordBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(red: 53.0 / 255.0, green: 38.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var registrateButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.setTitle("Регистрация".localiz().uppercased(), for: .normal)
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .bold)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onRegistrateClicked(sender:)), for: .touchUpInside)
        view.backgroundColor = UIColor.init(red: 138.0 / 255.0, green: 86.0 / 255.0, blue: 172.0 / 255.0, alpha: 1.0)
        view.layer.cornerRadius = 24.0
        return view
    }()
    
    var progressView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var errorView: UILabel = {
        let view = UILabel()
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 16)
        view.textColor = .red
        view.text = "Ошибка при попытке регистрации".localiz()
        view.font = .systemFont(ofSize: 14.0)
        view.textAlignment = .center
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var authorizationButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.setTitle("Авторизация".localiz(), for: .normal)
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .bold)
        view.setTitleColor(.lightGray, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onAuthorizationClicked(sender:)), for: .touchUpInside)
        view.layer.cornerRadius = 24.0
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        let safeArea = self.view.safeAreaLayoutGuide
        
        self.view.addSubview(loginView)
        self.view.addSubview(loginBorder)
        self.view.addSubview(emailView)
        self.view.addSubview(emailBorder)
        self.view.addSubview(passwordView)
        self.view.addSubview(passwordBorder)
        self.view.addSubview(confirmPasswordView)
        self.view.addSubview(confirmPasswordBorder)
        self.view.addSubview(registrateButton)
        self.view.addSubview(errorView)
        self.view.addSubview(progressView)
        self.view.addSubview(authorizationButton)
                
        self.passwordView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64.0).isActive = true
        self.passwordView.topAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -32.0).isActive = true
        self.passwordView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64.0).isActive = true
        self.passwordView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        self.passwordBorder.leftAnchor.constraint(equalTo: self.passwordView.leftAnchor).isActive = true
        self.passwordBorder.topAnchor.constraint(equalTo: self.passwordView.bottomAnchor).isActive = true
        self.passwordBorder.rightAnchor.constraint(equalTo: self.passwordView.rightAnchor).isActive = true
        self.passwordBorder.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        
        self.emailView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64.0).isActive = true
        self.emailView.bottomAnchor.constraint(equalTo: self.passwordView.topAnchor, constant: -16.0).isActive = true
        self.emailView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64.0).isActive = true
        self.emailView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true

        self.emailBorder.leftAnchor.constraint(equalTo: self.emailView.leftAnchor).isActive = true
        self.emailBorder.topAnchor.constraint(equalTo: self.emailView.bottomAnchor).isActive = true
        self.emailBorder.rightAnchor.constraint(equalTo: self.emailView.rightAnchor).isActive = true
        self.emailBorder.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        
        self.loginView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64.0).isActive = true
        self.loginView.bottomAnchor.constraint(equalTo: self.emailView.topAnchor, constant: -16.0).isActive = true
        self.loginView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64.0).isActive = true
        self.loginView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true

        self.loginBorder.leftAnchor.constraint(equalTo: self.loginView.leftAnchor).isActive = true
        self.loginBorder.topAnchor.constraint(equalTo: self.loginView.bottomAnchor).isActive = true
        self.loginBorder.rightAnchor.constraint(equalTo: self.loginView.rightAnchor).isActive = true
        self.loginBorder.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        
        self.confirmPasswordView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64.0).isActive = true
        self.confirmPasswordView.topAnchor.constraint(equalTo: self.passwordView.bottomAnchor, constant: 16.0).isActive = true
        self.confirmPasswordView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -64.0).isActive = true
        self.confirmPasswordView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true

        self.confirmPasswordBorder.leftAnchor.constraint(equalTo: self.confirmPasswordView.leftAnchor).isActive = true
        self.confirmPasswordBorder.topAnchor.constraint(equalTo: self.confirmPasswordView.bottomAnchor).isActive = true
        self.confirmPasswordBorder.rightAnchor.constraint(equalTo: self.confirmPasswordView.rightAnchor).isActive = true
        self.confirmPasswordBorder.heightAnchor.constraint(equalToConstant: 2.0).isActive = true

        self.registrateButton.leftAnchor.constraint(equalTo: self.emailView.leftAnchor).isActive = true
        self.registrateButton.topAnchor.constraint(equalTo: self.confirmPasswordView.bottomAnchor, constant: 48.0).isActive = true
        self.registrateButton.rightAnchor.constraint(equalTo: self.emailView.rightAnchor).isActive = true
        self.registrateButton.heightAnchor.constraint(equalToConstant: 48.0).isActive = true

        self.progressView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.progressView.topAnchor.constraint(equalTo: self.confirmPasswordView.bottomAnchor, constant: 48.0).isActive = true
        self.progressView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.progressView.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        self.progressView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true

        self.errorView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.errorView.topAnchor.constraint(equalTo: self.registrateButton.bottomAnchor, constant: 24.0).isActive = true
        self.errorView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.errorView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true

        self.authorizationButton.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.authorizationButton.topAnchor.constraint(equalTo: self.errorView.bottomAnchor, constant: 16.0).isActive = true
        self.authorizationButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.authorizationButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
    }
    
    func showProgress() {
        self.errorView.isHidden = true
        self.registrateButton.isHidden = true
        self.progressView.isHidden = false
        self.progressView.startAnimating()
    }
    
    func hideProgress() {
        self.progressView.stopAnimating()
        self.progressView.isHidden = true
        self.registrateButton.isHidden = false
    }
    
    func showError() {
        self.errorView.isHidden = false
    }
    
    func showSuccess() {
        let alertController = UIAlertController(title: "Поздравляем".localiz(), message: "Вы успешно зарегистрировались в системе".localiz(), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (alert: UIAlertAction) in
            self.leaveVC()
        }))
        self.present(alertController, animated: true)
    }
    
    func leaveVC() {
        dismiss(animated: false, completion: nil)
    }
    
    @objc func onRegistrateClicked(sender: UIButton) {
        if loginView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty == true || emailView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty == true || passwordView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty == true || confirmPasswordView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty == true {
                let alertController = UIAlertController(title: "Внимание".localiz(), message: "Заполните все поля".localiz(), preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ок".localiz(), style: .default, handler: nil))
                self.present(alertController, animated: true)
                return
        }
        
        if passwordView.text != confirmPasswordView.text {
            let alertController = UIAlertController(title: "Внимание".localiz(), message: "Пароли не совпадают".localiz(), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ок".localiz(), style: .default, handler: nil))
            self.present(alertController, animated: true)
            return
        }
        
        sendRegistrationRequest(login: loginView.text!, email: emailView.text!, password: passwordView.text!)
    }
    
    @objc func onAuthorizationClicked(sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    func sendRegistrationRequest(login: String, email: String, password: String) {
        self.showProgress()
        
        Alamofire.request(
            REGISTRATION_REQUEST,
            method: .post,
            encoding: "{\"username\":\"\(login)\",\"email\":\"\(email)\",\"password\":\"\(password)\"}",
            headers: headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                self.hideProgress()
                
                guard response.result.isSuccess else {
                    self.showError()
                    return
                }
                
                self.showSuccess()
            }
    }
}
