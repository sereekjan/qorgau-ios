//
//  SideMenuViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/24/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
import LanguageManager_iOS

class SideMenuViewController : UIViewController {
    
    var aboutButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .medium)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(aboutClicked(sender:)), for: .touchUpInside)
        return view
    }()
        
    var teamButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .medium)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(teamClicked(sender:)), for: .touchUpInside)
        return view
    }()

    var partnersButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .medium)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(partnersClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    var organizatorsButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 440.0, height: 24.0)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .medium)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(organizatorsClicked(sender:)), for: .touchUpInside)
        return view
    }()
        
    var humanLawsButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .medium)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(humanLawsClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    var dividerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()
        
    var changeLanguageButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.contentHorizontalAlignment = .left
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .medium)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(changeLangClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let safeGuide = self.view.safeAreaLayoutGuide
        
        self.view.backgroundColor = .init(red: 36.0 / 255.0, green: 19.0 / 255.0, blue: 50.0 / 255.0, alpha: 1.0)
        
        self.view.addSubview(aboutButton)
        self.view.addSubview(teamButton)
        self.view.addSubview(partnersButton)
        self.view.addSubview(organizatorsButton)
        self.view.addSubview(humanLawsButton)
        self.view.addSubview(dividerView)
        self.view.addSubview(changeLanguageButton)
        
        self.aboutButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.aboutButton.topAnchor.constraint(equalTo: safeGuide.topAnchor).isActive = true
        self.aboutButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.aboutButton.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        self.aboutButton.setTitle("О нас".localiz(), for: .normal)
        
        self.teamButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.teamButton.topAnchor.constraint(equalTo: self.aboutButton.bottomAnchor).isActive = true
        self.teamButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.teamButton.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        self.teamButton.setTitle("Наша команда".localiz(), for: .normal)
        
        self.partnersButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.partnersButton.topAnchor.constraint(equalTo: self.teamButton.bottomAnchor).isActive = true
        self.partnersButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.partnersButton.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        self.partnersButton.setTitle("Партнеры".localiz(), for: .normal)
        
        self.organizatorsButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.organizatorsButton.topAnchor.constraint(equalTo: self.partnersButton.bottomAnchor).isActive = true
        self.organizatorsButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.organizatorsButton.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        self.organizatorsButton.setTitle("Организаторы".localiz(), for: .normal)
        
        self.humanLawsButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.humanLawsButton.topAnchor.constraint(equalTo: self.organizatorsButton.bottomAnchor).isActive = true
        self.humanLawsButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.humanLawsButton.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        self.humanLawsButton.setTitle("Права человека".localiz(), for: .normal)
        
        self.dividerView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.dividerView.topAnchor.constraint(equalTo: self.humanLawsButton.bottomAnchor, constant: 12.0).isActive = true
        self.dividerView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.dividerView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        self.changeLanguageButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.changeLanguageButton.topAnchor.constraint(equalTo: self.dividerView.bottomAnchor, constant: 8.0).isActive = true
        self.changeLanguageButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.changeLanguageButton.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        self.changeLanguageButton.setTitle("Сменить язык".localiz(), for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(langChanged(_:)), name: Notification.Name("LangChanged"), object: nil)
    }
    
    @objc func langChanged(_ notification: Notification) {
//        self.aboutButton.setTitle("О нас".localiz(), for: .normal)
        self.viewDidLoad()
    }
    
    @objc func aboutClicked(sender: UIButton?) {
        let aboutVC = MenuAboutViewController()
        aboutVC.filename = "russian_about".localiz()
        aboutVC.viewTitle = "О нас".localiz()
        aboutVC.modalPresentationStyle = .fullScreen
        present(aboutVC, animated: true, completion: nil)
    }
    
    @objc func teamClicked(sender: UIButton?) {
        let aboutVC = MenuTeamViewController()
        aboutVC.modalPresentationStyle = .fullScreen
        present(aboutVC, animated: true, completion: nil)
    }
    
    @objc func partnersClicked(sender: UIButton?) {
        let aboutVC = MenuAboutViewController()
        aboutVC.filename = "russian_partners".localiz()
        aboutVC.viewTitle = "Партнеры".localiz()
        aboutVC.modalPresentationStyle = .fullScreen
        present(aboutVC, animated: true, completion: nil)
    }
    
    @objc func organizatorsClicked(sender: UIButton?) {
        let aboutVC = MenuAboutViewController()
        aboutVC.filename = "russian_organizators".localiz()
        aboutVC.viewTitle = "Организаторы".localiz()
        aboutVC.modalPresentationStyle = .fullScreen
        present(aboutVC, animated: true, completion: nil)
    }
    
    @objc func humanLawsClicked(sender: UIButton?) {
        let aboutVC = MenuAboutViewController()
        aboutVC.filename = "russian_rights".localiz()
        aboutVC.viewTitle = "Права человека".localiz()
        aboutVC.modalPresentationStyle = .fullScreen
        present(aboutVC, animated: true, completion: nil)
    }
    
    @objc func changeLangClicked(sender: UIButton?) {
        let langAlert = UIAlertController(title: "Сменить язык".localiz(), message: nil, preferredStyle: .actionSheet)
        
        let russianLangAction = UIAlertAction(title: "Русский", style: .default, handler: { (alert: UIAlertAction) in
            LanguageManager.shared.currentLanguage = .ru
            NotificationCenter.default.post(name: Notification.Name(rawValue: "LangChanged"), object: nil)
        })
        let kazakhLangAction = UIAlertAction(title: "Казақша", style: .default, handler: { (alert: UIAlertAction) in
            LanguageManager.shared.currentLanguage = .de
            NotificationCenter.default.post(name: Notification.Name(rawValue: "LangChanged"), object: nil)
        })
        let cancelAction = UIAlertAction(title: "Отмена".localiz(), style: .cancel, handler: nil)
        
        langAlert.addAction(russianLangAction)
        langAlert.addAction(kazakhLangAction)
        langAlert.addAction(cancelAction)
        
        present(langAlert, animated: true, completion: nil)
    }
}
