//
//  Reply.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/24/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

struct Reply : Decodable {
    var id: Int
    var title: Field
    var content: Field
    var author: Int
    var date: String
}
