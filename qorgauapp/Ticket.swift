//
//  Ticket.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/22/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

struct Ticket : Decodable {
    var id: Int?
    var status: String?
    var type: String?
    var title: Field?
    var content: Field?
    var authorId: Int?
    var state: String?
    var date: String?
    
    enum CodingKeys : String, CodingKey {
        case id
        case status
        case type
        case title
        case content
        case authorId = "author"
        case state
        case date
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try! container.decode(Int.self, forKey: CodingKeys.id)
        self.status = try! container.decode(String.self, forKey: CodingKeys.status)
        self.type = try! container.decode(String.self, forKey: CodingKeys.type)
        self.title = try! container.decode(Field.self, forKey: CodingKeys.title)
        self.content = try! container.decode(Field.self, forKey: CodingKeys.content)
        self.authorId = try! container.decode(Int.self, forKey: CodingKeys.authorId)
        self.state = try! container.decode(String.self, forKey: CodingKeys.state)
        self.date = try! container.decode(String.self, forKey: CodingKeys.date)
    }
}
