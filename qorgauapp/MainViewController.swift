//
//  MainViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/24/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

class MainViewController : UITabBarController {
    
    var sideMenu: SideMenuNavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(sideMenuOpenClicked(_:)), name: Notification.Name(rawValue: "MenuOpen"), object: nil)

        sideMenu = SideMenuNavigationController(rootViewController: SideMenuViewController())
        sideMenu?.leftSide = true
        sideMenu?.presentationStyle = .menuSlideIn
        sideMenu?.presentationStyle.backgroundColor = .init(white: 1.0, alpha: 0.0)
        sideMenu?.statusBarEndAlpha = 0.0

        SideMenuManager.default.addPanGestureToPresent(toView: sideMenu!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: sideMenu!.view)
    }

    @objc func sideMenuOpenClicked(_ notification: Notification) {
        openSideMenu()
    }
    
    func openSideMenu() {
        if let menu = sideMenu {
            present(menu, animated: true, completion: nil)
        }
    }
}
