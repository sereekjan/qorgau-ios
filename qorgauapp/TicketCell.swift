//
//  TicketCell.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/22/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class TicketCell : UITableViewCell {
    
    let stringToDateFormatter = DateFormatter()
    let dateFormatter = DateFormatter()
    
    var ticket: Ticket?
    var indexOfRow: Int?
    
    var rowIndexView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 26, weight: .bold)
        view.textColor = .init(white: 1.0, alpha: 0.26)
        view.textAlignment = .right
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var titleView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 26, weight: .bold)
        view.textColor = .white
        view.contentMode = .bottom
        view.numberOfLines = 3
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var descriptionView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14.0)
        view.textColor = .init(white: 1.0, alpha: 0.72)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        stringToDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        dateFormatter.doesRelativeDateFormatting = true
        
        self.addSubview(self.rowIndexView)
        self.addSubview(self.titleView)
        self.addSubview(self.descriptionView)
            
        self.rowIndexView.topAnchor.constraint(equalTo: self.topAnchor, constant: 22.0).isActive = true
        self.rowIndexView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -24.0).isActive = true
        self.rowIndexView.heightAnchor.constraint(equalToConstant: 28.0).isActive = true
        self.rowIndexView.widthAnchor.constraint(equalToConstant: 64.0).isActive = true
        
        self.titleView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 48.0).isActive = true
        self.titleView.topAnchor.constraint(equalTo: self.rowIndexView.bottomAnchor, constant: 12.0).isActive = true
        self.titleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -24.0).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        
        self.descriptionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 48.0).isActive = true
        self.descriptionView.topAnchor.constraint(equalTo: self.titleView.bottomAnchor, constant: 8.0).isActive = true
        self.descriptionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -24.0).isActive = true
        self.descriptionView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        self.descriptionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16.0).isActive = true
    }
    
    override func layoutSubviews() {
        dateFormatter.locale = Locale(identifier: "locale_identifier".localiz())
        
        let row = (indexOfRow ?? 0) + 1
        
        switch row % 3 {
            case 0:
                self.backgroundColor = UIColor.init(red: 138.0 / 255.0, green: 86.0 / 255.0, blue: 172.0 / 255.0, alpha: 1.0)
            case 1:
                self.backgroundColor = UIColor.init(red: 41.0 / 255.0, green: 115.0 / 255.0, blue: 212.0 / 255.0, alpha: 1.0)
            case 2:
                self.backgroundColor = UIColor.init(red: 179.0 / 255.0, green: 49.0 / 255.0, blue: 161.0 / 255.0, alpha: 1.0)
            default:
                break
        }
        
        self.rowIndexView.text = row < 10 ? "0\(row)" : "\(row)"
        self.titleView.text = ticket?.title?.rendered ?? "Нет данных".localiz()
        
        
        self.descriptionView.text = "Костанай - \(dateFormatter.string(from: stringToDateFormatter.date(from: ticket?.date ?? "")!.addSixHours()))"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension Date {
    func addSixHours() -> Date {
        return Calendar.current.date(byAdding: .hour, value: 6, to: self)!
    }
}
