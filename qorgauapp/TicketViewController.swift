//
//  TicketViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/24/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import WebKit

class TicketViewController : UIViewController {
    
    let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"

    var ticket: Ticket?
//    private var TICKET_REQUEST: String {
//        get {
//            return "https://qorgau.factum.agency/wp-json/wpas-api/v1/tickets/\(ticketId)"
//        }
//    }
    
//    private var headers: HTTPHeaders {
//        get {
//            return [
//                "Authorization": "Bearer \(UserManager.shared.accessToken ?? "")",
//                "Accept": "application/json",
//                "Content-Type": "application/json"
//            ]
//        }
//    }
    
    var titleView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 22, weight: .bold)
        view.textColor = .black
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var descriptionView: WKWebView = {
        let view = WKWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var openChatButton: UIButton = {
        let view = UIButton()
        view.layer.cornerRadius = 28.0
        view.setImage(UIImage(named: "ic_chat_24px"), for: .normal)
        view.backgroundColor = .init(red: 36.0 / 255.0, green: 19.0 / 255.0, blue: 50.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(openChatClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let safeAreaGuide = self.view.safeAreaLayoutGuide
        
        self.view.backgroundColor = .white
        
        self.view.addSubview(self.titleView)
        self.view.addSubview(self.descriptionView)
        self.view.addSubview(self.openChatButton)
        
        self.titleView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.titleView.topAnchor.constraint(equalTo: safeAreaGuide.topAnchor, constant: 40.0).isActive = true
        self.titleView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.descriptionView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 30.0).isActive = true
        self.descriptionView.topAnchor.constraint(equalTo: self.titleView.bottomAnchor, constant: 32.0).isActive = true
        self.descriptionView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -30.0).isActive = true
        self.descriptionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -16.0).isActive = true
        
        self.openChatButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16.0).isActive = true
        self.openChatButton.bottomAnchor.constraint(equalTo: safeAreaGuide.bottomAnchor, constant: -16.0).isActive = true
        self.openChatButton.heightAnchor.constraint(equalToConstant: 56.0).isActive = true
        self.openChatButton.widthAnchor.constraint(equalToConstant: 56.0).isActive = true
        
        titleView.text = ticket?.title?.rendered
        if let html = ticket?.content?.rendered {
            descriptionView.loadHTMLString(headerString + html, baseURL: nil)
        }
    }
    
    @objc func openChatClicked(sender: UIButton?) {
        let chatVC = TicketChatViewController()
        chatVC.ticketId = self.ticket?.id ?? 0
        chatVC.modalPresentationStyle = .fullScreen
        present(chatVC, animated: true, completion: nil)
    }
}
