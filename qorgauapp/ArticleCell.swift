//
//  ArticleCell.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/29/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit

class ArticleCell : UITableViewCell {
    
    var article: Article?
    let stringToDateFormatter = DateFormatter()
    let dateFormatter = DateFormatter()
    
    var bgImageView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 16.0
        view.backgroundColor = .init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var titleView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 13.0, weight: .semibold)
        view.textColor = .white
        view.numberOfLines = 2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = .init(red: 165.0 / 255.0, green: 82.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var timeView: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 10.0)
        view.textColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        stringToDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        dateFormatter.doesRelativeDateFormatting = true
        
        self.addSubview(self.bgImageView)
        self.addSubview(self.titleView)
        self.addSubview(self.dividerView)
        self.addSubview(self.timeView)
        
        self.bgImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 36.0).isActive = true
        self.bgImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 18.0).isActive = true
        self.bgImageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -36.0).isActive = true
        self.bgImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12.0).isActive = true
        self.bgImageView.heightAnchor.constraint(equalToConstant: 196.0).isActive = true
        
        self.timeView.leftAnchor.constraint(equalTo: self.bgImageView.leftAnchor, constant: 16.0).isActive = true
        self.timeView.bottomAnchor.constraint(equalTo: self.bgImageView.bottomAnchor, constant: -16.0).isActive = true
        self.timeView.rightAnchor.constraint(equalTo: self.bgImageView.rightAnchor, constant: -16.0).isActive = true
        self.timeView.heightAnchor.constraint(equalToConstant: 12.0).isActive = true
        
        self.dividerView.leftAnchor.constraint(equalTo: self.bgImageView.leftAnchor, constant: 16.0).isActive = true
        self.dividerView.bottomAnchor.constraint(equalTo: self.timeView.topAnchor, constant: -5.0).isActive = true
        self.dividerView.heightAnchor.constraint(equalToConstant: 3.0).isActive = true
        self.dividerView.widthAnchor.constraint(equalToConstant: 18.0).isActive = true
        
        self.titleView.leftAnchor.constraint(equalTo: self.bgImageView.leftAnchor, constant: 16.0).isActive = true
        self.titleView.bottomAnchor.constraint(equalTo: self.dividerView.topAnchor, constant: -6.0).isActive = true
        self.titleView.rightAnchor.constraint(equalTo: self.bgImageView.rightAnchor, constant: -16.0).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
    }
    
    override func layoutSubviews() {
        dateFormatter.locale = Locale(identifier: "locale_identifier".localiz())
        
        if let article = self.article {
            titleView.text = article.title.rendered
            timeView.text = dateFormatter.string(from: stringToDateFormatter.date(from: article.date)!.addSixHours())
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
