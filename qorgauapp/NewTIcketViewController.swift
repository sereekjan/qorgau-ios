//
//  NewTIcketViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/23/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class NewTicketViewController : UIViewController {
    
    private let NEW_TICKET_REQUEST = "https://qorgau.factum.agency/wp-json/wpas-api/v1/tickets"
    private var headers: HTTPHeaders {
        get {
            return [
                "Authorization": "Bearer \(UserManager.shared.accessToken ?? "")",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
        }
    }
    
    var titleView: UILabel = {
        let view = UILabel()
        view.text = "Создать тикет".localiz()
        view.font = .systemFont(ofSize: 26, weight: .bold)
        view.textColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var topicFrame: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 26.0
        view.backgroundColor = .init(red: 242.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var topicTextField: UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var descriptionFrame: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 26.0
        view.backgroundColor = .init(red: 242.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var descriptionTextField: UITextView = {
        let view = UITextView()
        view.placeholder = "Ваше сообщение".localiz()
        view.font = .systemFont(ofSize: 16.0)
        view.backgroundColor = .init(red: 242.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var attachFrame: UIView = {
        let view = UIView()
        view.backgroundColor = .init(red: 242.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var attachFormatsView: UILabel = {
        let view = UILabel()
        view.text = "Это текст о поддерживаемых форматах и размерах файлов для загрузки"
        view.textColor = .init(white: 0.0, alpha: 0.52)
        view.font = .systemFont(ofSize: 14.0)
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var attachFileFrame: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 26.0
        view.backgroundColor = .init(red: 165.0 / 255.0, green: 82.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var attachFileView: UILabel = {
        let view = UILabel()
        view.textColor = .white
        view.font = .systemFont(ofSize: 15.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var attachImageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ic_upload_24px")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var createButton: UIButton = {
        let view = UIButton()
        view.frame = CGRect(x: 0, y: 0, width: 240.0, height: 24.0)
        view.setTitle("Создать тикет".localiz().uppercased(), for: .normal)
        view.titleLabel?.font = .systemFont(ofSize: 16.0, weight: .bold)
        view.setTitleColor(.white, for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(newTicketClicked(sender:)), for: .touchUpInside)
        view.backgroundColor = UIColor.init(red: 138.0 / 255.0, green: 86.0 / 255.0, blue: 172.0 / 255.0, alpha: 1.0)
        view.layer.cornerRadius = 24.0
        return view
    }()
    
    var progressView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        renderView()
    }
    
    func renderView() {
        self.view.backgroundColor = .white
        self.view.addSubview(self.titleView)
        
        self.view.addSubview(self.topicFrame)
        self.topicFrame.addSubview(self.topicTextField)
        
        self.view.addSubview(self.descriptionFrame)
        self.descriptionFrame.addSubview(self.descriptionTextField)
        
        self.view.addSubview(self.attachFrame)
        self.attachFrame.addSubview(self.attachFormatsView)
        
        self.view.addSubview(self.attachFileFrame)
        self.attachFileFrame.addSubview(self.attachFileView)
        self.attachFileFrame.addSubview(self.attachImageView)
        
        self.view.addSubview(self.createButton)
        self.view.addSubview(self.progressView)
        
        self.titleView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 64.0).isActive = true
        self.titleView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 40.0).isActive = true
        self.titleView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 64.0).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 28.0).isActive = true
        
        self.topicFrame.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.topicFrame.topAnchor.constraint(equalTo: self.titleView.bottomAnchor, constant: 40.0).isActive = true
        self.topicFrame.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.topicFrame.heightAnchor.constraint(equalToConstant: 52.0).isActive = true
        
        self.topicTextField.leftAnchor.constraint(equalTo: self.topicFrame.leftAnchor, constant: 20.0).isActive = true
        self.topicTextField.topAnchor.constraint(equalTo: self.topicFrame.topAnchor, constant: 12.0).isActive = true
        self.topicTextField.rightAnchor.constraint(equalTo: self.topicFrame.rightAnchor, constant: -20.0).isActive = true
        self.topicTextField.bottomAnchor.constraint(equalTo: self.topicFrame.bottomAnchor, constant: -12.0).isActive = true
        self.topicTextField.attributedPlaceholder = NSAttributedString(string: "Введите тему", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        self.descriptionFrame.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.descriptionFrame.topAnchor.constraint(equalTo: self.topicFrame.bottomAnchor, constant: 28.0).isActive = true
        self.descriptionFrame.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.descriptionFrame.heightAnchor.constraint(equalToConstant: 144.0).isActive = true
        
        self.descriptionTextField.leftAnchor.constraint(equalTo: self.descriptionFrame.leftAnchor, constant: 16.0).isActive = true
        self.descriptionTextField.topAnchor.constraint(equalTo: self.descriptionFrame.topAnchor, constant: 12.0).isActive = true
        self.descriptionTextField.rightAnchor.constraint(equalTo: self.descriptionFrame.rightAnchor, constant: -20.0).isActive = true
        self.descriptionTextField.bottomAnchor.constraint(equalTo: self.descriptionFrame.bottomAnchor, constant: -12.0).isActive = true
        
        self.attachFrame.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.attachFrame.topAnchor.constraint(equalTo: self.descriptionFrame.bottomAnchor, constant: 54.0).isActive = true
        self.attachFrame.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.attachFrame.heightAnchor.constraint(equalToConstant: 108.0).isActive = true
        
        self.attachFormatsView.leftAnchor.constraint(equalTo: self.attachFrame.leftAnchor, constant: 24.0).isActive = true
        self.attachFormatsView.topAnchor.constraint(equalTo: self.attachFrame.topAnchor, constant: 36.0).isActive = true
        self.attachFormatsView.rightAnchor.constraint(equalTo: self.attachFrame.rightAnchor, constant: -24.0).isActive = true
        self.attachFormatsView.bottomAnchor.constraint(equalTo: self.attachFrame.bottomAnchor, constant: -16.0).isActive = true
        
        self.attachFileFrame.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.attachFileFrame.centerYAnchor.constraint(equalTo: self.attachFrame.topAnchor).isActive = true
        self.attachFileFrame.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.attachFileFrame.heightAnchor.constraint(equalToConstant: 52.0).isActive = true
        
        self.attachFileView.leftAnchor.constraint(equalTo: self.attachFileFrame.leftAnchor, constant: 24.0).isActive = true
        self.attachFileView.topAnchor.constraint(equalTo: self.attachFileFrame.topAnchor, constant: 14.0).isActive = true
        self.attachFileView.bottomAnchor.constraint(equalTo: self.attachFileFrame.bottomAnchor, constant: -14.0).isActive = true
        self.attachFileView.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
        self.attachFileView.attributedText = NSAttributedString(string: "Прикрепить файлы".localiz(), attributes: [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
        
        self.attachImageView.centerYAnchor.constraint(equalTo: self.attachFileFrame.centerYAnchor).isActive = true
        self.attachImageView.rightAnchor.constraint(equalTo: self.attachFileFrame.rightAnchor, constant: -20.0).isActive = true
        self.attachImageView.widthAnchor.constraint(equalToConstant: 16.0).isActive = true
        self.attachImageView.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        
        self.createButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 36.0).isActive = true
        self.createButton.topAnchor.constraint(equalTo: self.attachFrame.bottomAnchor, constant: 36.0).isActive = true
        self.createButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -36.0).isActive = true
        self.createButton.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.progressView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.progressView.centerYAnchor.constraint(equalTo: self.createButton.centerYAnchor).isActive = true
        self.progressView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        self.progressView.widthAnchor.constraint(equalToConstant: 48.0).isActive = true
    }
    
    @objc func newTicketClicked(sender: UIButton?) {
        if self.topicTextField.text!.isEmpty == true || self.descriptionTextField.text.isEmpty {
            let alertController = UIAlertController(title: "Внимание".localiz(), message: "Заполните все поля".localiz(), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ок".localiz(), style: .default))
            self.present(alertController, animated: true)
        } else {
            sendCreateTicketRequest(title: self.topicTextField.text!, content: self.descriptionTextField.text)
        }
    }
    
    private func sendCreateTicketRequest(title: String, content: String) {
        self.showProgress()
        let headers = self.headers
        
        Alamofire.request(
            NEW_TICKET_REQUEST,
            method: .post,
            encoding: "{\"title\":\"\(title)\",\"content\":\"\(content)\"}",
            headers: headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                self.hideProgress()
                
                guard response.result.isSuccess else {
                    debugPrint(response.error)
                    self.showError()
                    return
                }
                
                do {
                    let ticket = try JSONDecoder().decode(Ticket.self, from: response.result.value!)
                    debugPrint(ticket)
                    self.showSuccess()
                } catch { }
                
            }
    }
    
    private func showProgress() {
        self.createButton.isHidden = true
        self.progressView.isHidden = false
        self.progressView.startAnimating()
    }
    
    private func hideProgress() {
        self.progressView.stopAnimating()
        self.progressView.isHidden = true
        self.createButton.isHidden = false
    }
    
    private func showError() {
        let alertController = UIAlertController(title: "Внимание".localiz(), message: "Не удалось создать тикет\nПопробуйте еще раз".localiz(), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: .default))
        self.present(alertController, animated: true)
    }
    
    private func showSuccess() {
        let alertController = UIAlertController(title: "Внимание".localiz(), message: "Ваш тикет был успешно создан".localiz(), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ок".localiz(), style: .default, handler: { (alert: UIAlertAction) in
            self.leaveVC()
        }))
        self.present(alertController, animated: true)
    }
    
    private func leaveVC() {
        self.dismiss(animated: true)
    }
}

extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height

            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
}
