//
//  TicketChatViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/24/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class TicketChatViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var MESSAGES_REQUEST: String {
        get {
            return "https://qorgau.factum.agency/wp-json/wpas-api/v1/tickets/\(ticketId)/replies"
        }
    }
    
    private var NEW_MESSAGE_REQUEST: String {
        get {
            return "https://qorgau.factum.agency/wp-json/wpas-api/v1/tickets/\(ticketId)/replies"
        }
    }
    
    private var headers: HTTPHeaders {
        get {
            return [
                "Authorization": "Bearer \(UserManager.shared.accessToken ?? "")",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
        }
    }
    
    let identifier = "ReplyCell"
    var ticketId: Int = 0
    var replies: [Reply] = []
    
    var bottomConstraint: NSLayoutConstraint?
    
    var backButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "ic_arrow_back"), for: .normal)
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onBackClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    var titleView: UILabel = {
        let view = UILabel()
        view.text = "Чат".localiz()
        view.font = .systemFont(ofSize: 26.0, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var tableView: UITableView = {
        let view = UITableView()
        view.separatorColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var bottomView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var messageView: UITextField = {
        let view = UITextField()
        view.font = .systemFont(ofSize: 14.0)
        view.placeholder = "Введите сообщение".localiz()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.autocapitalizationType = .sentences
        return view
    }()
    
    var sendButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage(named: "ic_send"), for: .normal)
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onSendClicked(sender:)), for: .touchUpInside)
        return view
    }()
    
    var bottomDivider: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var refreshControl: UIRefreshControl = {
        let view = UIRefreshControl()
        return view
    }()
    
    var progressView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        renderView()
        loadMessages()
    }
    
    @objc func onBackClicked(sender: UIButton?) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onSendClicked(sender: UIButton?) {
        if self.messageView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty == true {
            let alertController = UIAlertController(title: "Внимание".localiz(), message: "Не удалось отправить сообщение\nПопробуйте еще раз".localiz(), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ок", style: .default))
            self.present(alertController, animated: true)
        } else {
            sendMessage(message: self.messageView.text!)
        }
    }
    
    func renderView() {
        let safeAreaGuide = self.view.safeAreaLayoutGuide
        
        self.view.backgroundColor = .white
        
        self.view.addSubview(self.backButton)
        self.view.addSubview(self.titleView)
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.bottomView)
        self.view.addSubview(self.bottomDivider)
        
        self.bottomView.addSubview(self.messageView)
        self.bottomView.addSubview(self.sendButton)
        self.bottomView.addSubview(self.progressView)
        
        self.backButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 24.0).isActive = true
        self.backButton.topAnchor.constraint(equalTo: safeAreaGuide.topAnchor, constant: 28.0).isActive = true
        self.backButton.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
        self.backButton.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
        
        self.titleView.leftAnchor.constraint(equalTo: self.backButton.rightAnchor, constant: 24.0).isActive = true
        self.titleView.topAnchor.constraint(equalTo: safeAreaGuide.topAnchor, constant: 24.0).isActive = true
        self.titleView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -24.0).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.topAnchor.constraint(equalTo: self.titleView.bottomAnchor, constant: 16.0).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.bottomDivider.topAnchor).isActive = true
        self.tableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(refreshPulled(sender:)), for: .valueChanged)
        self.tableView.register(ReplyCell.self, forCellReuseIdentifier: identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.bottomConstraint = NSLayoutConstraint(item: self.bottomView, attribute: .bottom, relatedBy: .equal, toItem: safeAreaGuide, attribute: .bottom, multiplier: 1, constant: 0)
        
        self.bottomView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.bottomView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.view.addConstraint(bottomConstraint!)
        self.bottomView.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
        
        self.bottomDivider.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.bottomDivider.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.bottomDivider.bottomAnchor.constraint(equalTo: self.bottomView.topAnchor).isActive = true
        self.bottomDivider.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        self.messageView.leftAnchor.constraint(equalTo: self.bottomView.leftAnchor, constant: 16.0).isActive = true
        self.messageView.topAnchor.constraint(equalTo: self.bottomView.topAnchor, constant: 4.0).isActive = true
        self.messageView.rightAnchor.constraint(equalTo: self.sendButton.leftAnchor, constant: -16.0).isActive = true
        self.messageView.bottomAnchor.constraint(equalTo: self.bottomView.bottomAnchor, constant: -4.0).isActive = true
        
        self.sendButton.centerYAnchor.constraint(equalTo: self.bottomView.centerYAnchor).isActive = true
        self.sendButton.rightAnchor.constraint(equalTo: self.bottomView.rightAnchor, constant: -12.0).isActive = true
        self.sendButton.heightAnchor.constraint(equalToConstant: 28.0).isActive = true
        self.sendButton.widthAnchor.constraint(equalToConstant: 28.0).isActive = true
        
        self.progressView.centerYAnchor.constraint(equalTo: self.bottomView.centerYAnchor).isActive = true
        self.progressView.rightAnchor.constraint(equalTo: self.bottomView.rightAnchor, constant: -12.0).isActive = true
        self.progressView.heightAnchor.constraint(equalToConstant: 28.0).isActive = true
        self.progressView.widthAnchor.constraint(equalToConstant: 28.0).isActive = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardNotification(notification: Notification) {
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            print(keyboardFrame)
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
                }, completion: { (completed) in
                    
                    if isKeyboardShowing {
                        let indexPath = NSIndexPath(item: self.replies.count - 1, section: 0)
                        self.tableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
                    }
                    
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if replies.isEmpty {
            tableView.setEmptyView(title: "Список сообщений пуст".localiz(), message: "Напишите первое сообщение".localiz())
        } else {
            tableView.restore()
        }
        
        return replies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ReplyCell(style: .default, reuseIdentifier: identifier)
        
        let reply = replies[indexPath.row]
        cell.reply = reply
        cell.isAuthor = reply.author == UserManager.shared.userId
        cell.selectionStyle = .none
        cell.renderMessageFrame()
        
        return cell
    }
    
    @objc func refreshPulled(sender: UIButton?) {
        loadMessages()
    }
    
    private func loadMessages() {
        self.showProgress()
        Alamofire.request(
            MESSAGES_REQUEST,
            method: .get,
            headers: self.headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                self.hideProgress()
                
                guard response.result.isSuccess else {
                    debugPrint(response.error)
                    self.showError()
                    return
                }
                
                do {
                    let replies = try JSONDecoder().decode([Reply].self, from: response.result.value!)
                    debugPrint(replies)
                    self.replies = replies
                    self.tableView.reloadData()
                } catch { }
                
            }
    }
    
    private func sendMessage(message: String) {
        self.showSendProgress()
        Alamofire.request(
            MESSAGES_REQUEST,
            method: .post,
            encoding: "{\"ticket_id\":\(ticketId),\"content\":\"\(message)\"}",
            headers: self.headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                self.hideSendProgress()
                
                guard response.result.isSuccess else {
                    debugPrint(response.error)
                    let alertController = UIAlertController(title: "Внимание".localiz(), message: "Не удалось отправить сообщение\nПопробуйте еще раз".localiz(), preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Ок", style: .default))
                    self.present(alertController, animated: true)
                    return
                }
                
                do {
                    let reply = try JSONDecoder().decode(Reply.self, from: response.result.value!)
                    debugPrint(reply)
                    self.messageView.text = ""
                    self.replies.insert(reply, at: 0)
                    self.tableView.reloadData()
                } catch { }
                
            }
    }
    
    private func showProgress() {
        self.refreshControl.beginRefreshing()
    }
    
    private func hideProgress() {
        self.refreshControl.endRefreshing()
    }
    
    private func showError() {
        if self.replies.isEmpty {
            tableView.setEmptyView(title: "Ошибка при загрузке данных".localiz(), message: "Попробуйте еще раз".localiz())
        } else {
            let alertController = UIAlertController(title: "Внимание".localiz(), message: "Ошибка при загрузке данных\nПопробуйте еще раз".localiz(), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ок".localiz(), style: .default))
            self.present(alertController, animated: true)
        }
    }
    
    private func showSendProgress() {
        self.sendButton.isHidden = true
        self.progressView.isHidden = false
        self.progressView.startAnimating()
    }
    
    private func hideSendProgress() {
        self.progressView.stopAnimating()
        self.progressView.isHidden = true
        self.sendButton.isHidden = false
    }
}

extension UITableView {
    func setEmptyView(title: String, message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension UILabel {
    func getHeightForText(_ text: NSMutableAttributedString) -> CGFloat {
        let view = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
        
        view.attributedText = text
        view.font = self.font
        view.numberOfLines = 0
        view.sizeToFit()
        view.lineBreakMode = .byWordWrapping
        
        let newHeight = view.frame.height
        view.removeFromSuperview()
        
        return newHeight
    }
}
