//
//  VideosViewController.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/29/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftSoup

class VideosViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cellIdentifier = "VideoCell"
    
    private var VIDEOS_REQUEST: String {
        get {
            return "https://qorgau.factum.agency/wp-json/wp/v2/posts?categories=\(categoryId)"
        }
    }
    
    private var categoryId: String {
        get {
            return "videos_category_id".localiz()
        }
    }
    
    private var headers: HTTPHeaders {
        get {
            return [
                "Authorization": "Bearer \(UserManager.shared.accessToken ?? "")",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
        }
    }
    
    var videos: [Video] = []
        
    var menuButton: UIButton = {
        var view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setImage(UIImage(named: "ic_sort_black_24px"), for: .normal)
        return view
    }()
    
    var headerView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var titleView: UILabel = {
        var view = UILabel()
        view.font = .systemFont(ofSize: 24.0, weight: .bold)
        view.textColor = UIColor.init(red: 102.0 / 255.0, green: 51.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var refreshControl: UIRefreshControl = {
        let view = UIRefreshControl()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(langChanged(_:)), name: Notification.Name("LangChanged"), object: nil)
        
        self.renderView()
        self.loadVideos()
    }
    
    private func renderView() {
        self.view.backgroundColor = .white
        
        let safeGuide = self.view.safeAreaLayoutGuide
        
        self.view.addSubview(self.headerView)
        self.headerView.addSubview(self.menuButton)
        self.headerView.addSubview(self.titleView)
        self.view.addSubview(self.tableView)
        
        self.headerView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.headerView.topAnchor.constraint(equalTo: safeGuide.topAnchor).isActive = true
        self.headerView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.headerView.heightAnchor.constraint(equalToConstant: 64.0).isActive = true
        
        self.menuButton.leftAnchor.constraint(equalTo: self.headerView.leftAnchor, constant: 16.0).isActive = true
        self.menuButton.bottomAnchor.constraint(equalTo: self.headerView.bottomAnchor, constant: -16.0).isActive = true
        self.menuButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
        self.menuButton.addTarget(self, action: #selector(onMenuClicked(sender:)), for: .touchUpInside)
        
        self.titleView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.titleView.centerYAnchor.constraint(equalTo: self.menuButton.centerYAnchor).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 26.0).isActive = true
        self.titleView.text = "Видео".localiz()
        
        self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.tableView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor).isActive = true
        self.tableView.register(VideoCell.self, forCellReuseIdentifier: cellIdentifier)
        self.tableView.refreshControl = self.refreshControl
        self.tableView.separatorColor = .white
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.refreshControl.addTarget(self, action: #selector(updateButtonClicked(sender:)), for: .valueChanged)
    }
    
    @objc func updateButtonClicked(sender: UIButton?) {
        self.loadVideos()
    }
    
    @objc func langChanged(_ notification: Notification) {
        self.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.videos.isEmpty {
            tableView.setEmptyView(title: "Нет данных".localiz(), message: "Попробуйте позже".localiz())
        } else {
            tableView.restore()
        }
        
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! VideoCell
        
        cell.selectionStyle = .none
        cell.video = videos[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = videos[indexPath.row]
        
        do {
            let doc: Document = try SwiftSoup.parse(video.content.rendered)
            
            let iframeTag = try doc.select("iframe").first()
            if let videoLink = try iframeTag?.attr("src") {
                UIApplication.shared.open(URL(string: videoLink)!)
            }
        } catch { }
    }
    
    @objc func onMenuClicked(sender: UIButton?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MenuOpen"), object: nil)
    }
    
    
    private func loadVideos() {
        self.showProgress()
        Alamofire.request(
            VIDEOS_REQUEST,
            method: .get,
            headers: self.headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                self.hideProgress()
                
                guard response.result.isSuccess else {
                    debugPrint(response.error)
                    self.showError()
                    return
                }
                
                do {
                    let videos = try JSONDecoder().decode([Video].self, from: response.result.value!)
                    debugPrint(videos)
                    
                    self.videos = videos
                    self.tableView.reloadData()
                } catch { }
            }
    }
    
    private func showProgress() {
        self.refreshControl.beginRefreshing()
    }
    
    private func hideProgress() {
        self.refreshControl.endRefreshing()
    }
    
    private func showError() {
        tableView.setEmptyView(title: "Ошибка при загрузке данных".localiz(), message: "Попробуйте еще раз".localiz())
    }
}
