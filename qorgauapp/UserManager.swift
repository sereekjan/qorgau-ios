//
//  UserManager.swift
//  qorgauapp
//
//  Created by Sereek Jan on 11/21/19.
//  Copyright © 2019 Qorgau. All rights reserved.
//

import Foundation

class UserManager {
    
    static let shared = UserManager()
    
    private static let ACCESS_TOKEN = "access_token"
    private static let USER_ID = "user_id"
    private static let USER_LOGIN = "user_login"
    private static let USER_EMAIL = "user_email"
    
    func isAuthorized() -> Bool {
        return accessToken != nil
    }
    
    var accessToken: String? {
        get {
            return UserDefaults.standard.string(forKey: UserManager.ACCESS_TOKEN)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserManager.ACCESS_TOKEN)
        }
    }
    
    var userId: Int? {
        get {
            return UserDefaults.standard.integer(forKey: UserManager.USER_ID)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserManager.USER_ID)
        }
    }
    
    var userLogin: String? {
        get {
            return UserDefaults.standard.string(forKey: UserManager.USER_LOGIN)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserManager.USER_LOGIN)
        }
    }
    
    var userEmail: String? {
        get {
            return UserDefaults.standard.string(forKey: UserManager.USER_EMAIL)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserManager.USER_EMAIL)
        }
    }
    
    func clearData() {
        accessToken = nil
        userId = nil
        userLogin = nil
        userEmail = nil
    }
}
